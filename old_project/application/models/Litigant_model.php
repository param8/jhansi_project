<?php 

class Litigant_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}


    public function store($data){
    return $this->db->insert('litigants',$data);

    }

    public function get_all_litigants($condition){
        $this->db->select('*');
		$this->db->from('litigants');
		$this->db->where($condition);
        if(!empty($this->session->userdata('disputeNo'))){
        $this->db->where('dispute_no', $this->session->userdata('disputeNo'));
        }
        if(!empty($this->session->userdata('start_date'))){
            $start_date = date('d/m/Y', strtotime($this->session->userdata('start_date')));
            $end_date = date('d/m/Y', strtotime($this->session->userdata('end_date')));
            $this->db->where("admission_date BETWEEN '$start_date' AND '$end_date'");
        }
        if(!empty($this->session->userdata('village'))){
            $this->db->where('village_name', $this->session->userdata('village'));
        }
	    $this->db->order_by('id','desc');
		return $this->db->get()->result();
         //echo $this->db->last_query(); die;
    }

    public function get_litigant($condition){
        $this->db->select('*');
		$this->db->from('litigants');
		$this->db->where($condition);
	    $this->db->order_by('id','desc');
		return $this->db->get()->row();
        //echo $this->db->last_query();
    }

    public function update($data,$id){
        $this->db->where('id',$id);
       return $this->db->update('litigants',$data);
    }

    public function delete($id){
        $this->db->where('id',$id);
        return $this->db->delete('litigants');
    }

    public function store_remark($data){
        return $this->db->insert('litigant_remark',$data);
    }


}