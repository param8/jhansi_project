<?php 

class Block_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}


    public function store($data){
    return $this->db->insert('block',$data);

    }

    public function get_all_blocks($condition){
        $this->db->select('*');
		$this->db->from('block');
		$this->db->where($condition);
	    $this->db->order_by('id','desc');
		return $this->db->get()->result();
    }

    public function get_blocks($condition){
        $this->db->select('*');
		$this->db->from('block');
		$this->db->where($condition);
	    $this->db->order_by('id','desc');
		return $this->db->get()->row();
    }

    public function update($data,$id){
        $this->db->where('id',$id);
       return $this->db->update('block',$data);
        //echo $this->db->last_query(); die;
    }

    public function delete($id){
        $this->db->where('id',$id);
        return $this->db->delete('block');
    }

    function make_query($condition)
  {
	  $this->db->select('*');
    $this->db->from('block');
    $this->db->where($condition);
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->like('name', $_POST["search"]["value"]);
   }
   $this->db->order_by('id','desc');   
  }


    function make_datatables($condition){
        $this->make_query($condition,);
        if($_POST["length"] != -1)
        {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
        
    }

    function get_all_data($condition)
    {
      $this->db->select('*');
      $this->db->from('block');
      $this->db->where($condition);
  
     if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
     {
      $this->db->like('name', $_POST["search"]["value"]);
     }
     $this->db->order_by('id','desc');
         return $this->db->count_all_results();
    }

    
    function get_filtered_data($condition){
        $this->make_query($condition);
        $query = $this->db->get();
        return $query->num_rows();
        //echo $this->db->last_query();die;
    }


}