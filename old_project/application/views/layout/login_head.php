<!doctype html>
<html lang="en" class="dark-mode">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
    <title><?=$page_title?></title>

    <!-- manifest meta -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="manifest" href="manifest.json" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="<?=base_url('public/assets/img/favicon180.png')?>" sizes="180x180">
    <link rel="icon" href="<?=base_url('public/assets/img/favicon32.png')?>" sizes="32x32" type="image/png">
    <link rel="icon" href="<?=base_url('public/assets/img/favicon16.png')?>" sizes="16x16" type="image/png">

    <!-- Google fonts-->
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>

    <!-- bootstrap icons -->
    <link rel="stylesheet" href="<?=base_url('public/assets/bootstrap-icons.css')?>">

    <!-- swiper carousel css -->
    <link rel="stylesheet" href="<?=base_url('public/assets/vendor/swiper-7.3.1/swiper-bundle.min.css')?>">

    <!-- style css for this template -->
    <link href="<?=base_url('public/assets/scss/style.css')?>" rel="stylesheet" id="style">
</head>

<body class="d-flex flex-column h-100 sidebar-pushcontent sidebar-filled" data-sidebarstyle="sidebar-pushcontent">

    <!-- page loader -->
    <div class="container-fluid h-100 position-fixed loader-wrap bg-blur">
        <div class="row justify-content-center h-100">
            <div class="col-auto align-self-center text-center px-5 leaf">
                <h2 class="mb-1">Admin<b class="fw-bold"></b></h2>
                <div class="logo-square animated mb-4">
                    <div class="icon-logo">
                        <img src="<?=base_url('public/assets/img/logo-icon.png')?>" alt="" />
                    </div>
                </div>
                <!-- <p class="text-secondary small">Petal of flower being ready to <span class="text-gradient">blossom</span></p> -->

                <!-- <div class="dotslaoder">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div> -->
                <br>
            </div>
        </div>
    </div>
    <!-- page loader ends -->

    <!-- background -->
    <div class="coverimg h-100 w-100 top-0 start-0 position-absolute" id="image-daytime">
        <div class=""></div>
        <img src="<?=base_url('public/assets/img/bg-1.jpg')?>" alt="" class="w-100" />
    </div>
    <!-- background ends  -->

    <!-- header -->
    <header class="header">
        <!-- Fixed navbar -->
        <nav class="navbar fixed-top">
            <div class="container-fluid">
                <!-- <div class="col-auto">
                    <a href="landing.html" class="btn btn-link text-secondary text-center"><i class="bi bi-chevron-left"></i></a>
                </div> -->
                <!-- <div class="col-auto ms-2">
                    <a class="navbar-brand d-block" href="#">
                        <div class="row">
                            <div class="col-auto"><span class="logo-icon"><img src="<?=base_url('public/assets/img/logo-icon.png')?>" class="mx-100" alt="" /></span></div>
                            <div class="col ps-0 align-self-center">
                                <h5 class="fw-normal text-dark">AdminUX</h5>
                                <p class="small text-secondary">Mobile Dashboard UI</p>
                            </div>
                        </div>
                    </a>
                </div> -->
                <div class="col-auto ms-auto">
                    <button type="button" class="btn btn-link text-secondary text-center" id="addtohome"><i class="bi bi-cloud-download-fill me-0 me-lg-1"></i> <span class="d-none d-lg-inline-block">Install</span></button>
                </div>
            </div>
        </nav>
    </header>
    <!-- header ends -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

  <script>
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
</head>

<body class="hold-transition light-skin sidebar-mini theme-primary fixed">
   <div class="wrapper">
	 <div id="loader"></div>