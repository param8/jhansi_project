<main class="main mainheight" id="printable_div_id">
<div class="container">
<div class="row">
  <div class="col-12 col-sm-auto mb-2">
    <div class="rounded">
      <a type="button" class="btn btn-primary" onclick="printInvoice('printable_div_id');"><i class="bi bi-printer"></i> प्रिंट निकाले</a>                                        
      <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addVillageModal">Add Village
        </button> -->
    </div>
  </div>
  <div class="card-body p-0">
    <div id="case_panel" style="width:100%;">
      <table border="0" align="center" cellpadding="5" cellspacing="4" style="width: 100%; font-size: 14px;">
        <tr>
          <td align="center" colspan="4">
          </td>
        </tr>
        <tr>
          <td align="center" colspan="4">
            <strong style="font-size: 18px; color: #800000; text-decoration: underline;">वाद का
            सारांश</strong>
          </td>
        </tr>
        </br>
        <tr>
          <td align="center" colspan="4" style="font-size: 16px;">
            <span id="lbl_court_name"><strong>मण्डल: - </strong>&nbsp;&nbsp;झांसी&nbsp;&nbsp;<strong>जनपद: - </strong>&nbsp;&nbsp;झांसी&nbsp;&nbsp;<strong>तहसील: - </strong>&nbsp;&nbsp;झांसी&nbsp;&nbsp;<strong>न्यायालय: - </strong>&nbsp;&nbsp;तहसीलदार</span>
          </td>
        </tr>
        <tr>
          <td align="left" valign="top" style="width: 389px">
            <strong>वाद सं०:</strong>
          </td>
          <td align="left" valign="top" style="width: 1010px">
            <span id="lbl_case_no"><?=$litigant->old_dispute_no?></span><input type="hidden" name="hidden_act" id="hidden_act" value="75" />
          </td>
          <td align="left" style="width: 371px" valign="top">
            <strong>कंप्यूटरीकृत वाद सं०:</strong>
          </td>
          <td align="left" style="width: 659px" valign="top">
            <span id="lbl_new_case_no"><?=$litigant->dispute_no?></span>
          </td>
        </tr>
        <tr>
          <td align="left" valign="top" style="width: 389px">
            <strong>वादी / प्रतिवादी के नाम एवम पता:</strong>
          </td>
          <td align="left" valign="top" style="width: 1010px">
            <?=$litigant->plaintiffs_and_defendants?>
            <!-- <span id="txt_lbl_party">सीता देवी&nbsp;,&nbsp;सिविल लाईन तहसील व जिला झांसी 8887865876</span><br />
              <strong>बनाम</strong><br />
              <span id="txt_lbl_detail">श्री  अलोक&nbsp;,&nbsp;ओम शांति नगर तहसील व जिला झांसी 9415092900</span> -->
          </td>
          <td align="left" style="width: 371px" valign="top">
            <strong>वाद की स्थिति:</strong>
          </td>
          <td align="left" style="width: 659px" valign="top">
            <span id="lbl_status"><?=$litigant->scheduled_action?></span>
          </td>
        </tr>
        <tr>
          <td align="left" valign="top" style="width: 389px">
            <strong>वाद प्रकृति:</strong>
          </td>
          <td align="left" valign="top" style="width: 1010px">
            <span id="lbl_nature"><?=$litigant->type?></span>
          </td>
          <td align="left" style="width: 371px" valign="top">
            <strong>दाखिल करने का दिनांक:</strong>
          </td>
          <td align="left" style="width: 659px" valign="top">
            <span id="txt_file_dt"><?=$litigant->admission_date?></span>
          </td>
        </tr>
        <tr>
          <!-- <td align="left" valign="top" style="width: 389px">
            <span id="lbl_listing_dt" style="font-weight:bold;">अगला सुनवाई दिनांक:</span>
          </td>
          <td align="left" valign="top" style="width: 1010px">
            <span id="txt_list_dt">01-Jun-2023</span>&nbsp;&nbsp;
          </td> -->
          <td align="left" style="width: 371px" valign="top">
            <strong>अधिनियम, धारा:</strong>
          </td>
          <td align="left" style="width: 659px" valign="top">
            <span id="txt_act_sect_detail"><?=$litigant->act_section?></span>
          </td>
        </tr>
        <tr>
          <td align="left" valign="top" style="width: 389px">
            <div id="panel_village" style="width:279px;">
              <strong>गाँव और परगने का नाम:</strong>
            </div>
          </td>
          <td align="left" valign="top" style="width: 1010px">&nbsp;<span id="lbl_vilage"><strong>गाँव:-</strong> </span>
            <span id="lbl_div"> , <strong>परगने का नाम:-</strong><?=$litigant->village_name?>, झांसी</span>
          </td>
          <td align="left" style="width: 371px" valign="top">
          </td>
          <td align="left" style="width: 659px" valign="top">
            <br />
            <br />
          </td>
        </tr>
        <tr>
          <td align="left" valign="top" style="width: 389px;">
          </td>
          <td align="left" valign="top" colspan="3">
          </td>
        </tr>
      </table>
      <!-- <div id="pnl_disputed_land_details" style="width:100%;">
        <tr>
            <td align="left" valign="top" colspan="5">
                 <table cellpadding='1' align='center' width='70%' cellspacing='0' border='1'><tr>     <td colspan='10' align='center' valign='Top' style='font-size:16px;color:red'>&nbsp;<strong>वादग्रस्त भूमि का विवरण</strong>     </td></tr><tr>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;<strong>क्र सं0</strong>     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;<strong>जनपद</strong>     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;<strong>तहसील</strong>     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;<strong>परगना</strong>     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;<strong>ग्राम</strong>     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;<strong>खतौनी खाता संख्या</strong>     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;<strong>गाटा संख्या</strong>     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;<strong>गाटा यूनिक कोड</strong>     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;<strong>क्षेत्रफल</strong>     </td></tr><tr>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;1     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;झांसी     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;झांसी     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;झांसी     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;कोछांभावर     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;00648     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;1528     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;218663-1528-000012     </td>     <td align='center' valign='Top' style='font-size:14px;'>&nbsp;&nbsp;4.6950     </td></tr></table>
            </td>
        </tr>
        
        </div> -->
      <!-- <div id="panel_order_sheet" style="width:100%;">
        <tr>
            <td align="left" valign="top" colspan="5">
                
            </td>
        </tr>
        </div> -->
    </div>
  </div>
</div>
<script>
  function printInvoice(printable_div_id) {
    var printContent = document.getElementById(printable_div_id).innerHTML;
    var originalContent = document.body.innerHTML;
    document.body.innerHTML = printContent;
    window.print();
    document.body.innerHTML = originalContent;
  }
</script>