<main class="main mainheight">
        <div class="container">
            <!-- forms -->
            <h5 class="title"><?=$page_title?></h5>
        <form action="<?=base_url('Litigant/update')?>" method="post" id="litigantForm">
            <div class="row">
                <div class="col-12 col-md-6 col-xl-4 mb-4 mb-md-0 ">
                    <!-- was-validated class on form when data are submitted -->
                    <div>
                        <div class="form-group mb-3 position-relative">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                                <div class="form-floating">
                                    <input type="text" placeholder="type" name="type" id="type" value="<?=$litigant->type?>" class="form-control border-start-0">
                                    <label>वाद का प्रकार</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4">
                        <div class="form-group mb-3 position-relative check-valid is-invalid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                                <div class="form-floating">
                                    <input type="text" placeholder="Enter Old dispute no" name="old_dispute_no" id="old_dispute_no" value="<?=$litigant->old_dispute_no?>" class="form-control border-start-0">
                                    <label>पुराना वाद संख्या</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4">
                        <div class="form-group mb-3 position-relative check-valid is-invalid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                                <div class="form-floating">
                                    <input type="text" placeholder="Enter Dispute no" value="<?=$litigant->dispute_no?>" name="dispute_no" id="dispute_no" class="form-control border-start-0">
                                    <label>कम्प्यूटरीकृत वाद संख्या</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-4 mb-4 mb-md-0">
                    <!-- Form elements -->
                    <div class="mb-2">
                        <div class="form-group mb-3 position-relative check-valid is-invalid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                                <div class="form-floating">
                                    <input type="text" placeholder="Enter Act/section" value="<?=$litigant->act_section?>" name="act_section" id="act_section" class="form-control border-start-0">
                                    <label>अधिनियम धारा</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <div class="form-group mb-3 position-relative check-valid is-valid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                                <div class="form-floating">
                                    <input type="text" placeholder="Enter Village Name" name="village_name" id="village_name" value="<?=$litigant->village_name?>" class="form-control border-start-0" autofocus>
                                    <label>ग्राम का नाम</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <div class="form-group mb-3 position-relative check-valid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                                <div class="form-floating">
                                    <input type="text" placeholder="Enter Police Station" name="police_station" id="police_station" value="<?=$litigant->police_station?>" class="form-control border-start-0">
                                    <label>थाना</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="form-group mb-3 position-relative check-valid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class=" "></i></span>
                                <div class="form-floating">
                                    <input type="date" placeholder="Enter Admission Dsate" value="<?=$litigant->admission_date?>" name="admission_date" id="admission_date" class="form-control border-start-0">
                                    <label>दाखिला तिथि</label>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="feedback mb-3">
                            <div class="row">
                                <div class="col">
                                    <div class="check-strength" id="checksterngthdisplay">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <span class="small" id="textpassword"></span>
                                    <i class="bi bi-info-circle text-theme ms-1" data-bs-trigger="hover" data-bs-container="body" data-bs-toggle="popover" data-bs-placement="top" data-bs-content="Password should contain atleast 1 capital, 1 alphanumeric & min. 8 characters"></i>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-4 mb-4 mb-md-0">
                <div class="mb-2">
                        <div class="form-group mb-3 position-relative check-valid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                                <div class="form-floating">
                                    <input type="text" placeholder="enter Plaintiffs and defendants" name="plaintiffs_and_defendants" id="plaintiffs_and_defendants" value="<?=$litigant->plaintiffs_and_defendants?>" class="form-control border-start-0" id="password2">
                                    <label>वादी एवं प्रतिवादी</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <div class="form-group mb-3 position-relative check-valid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class="bi bi-person"></i></span>
                                <div class="form-floating">
                                    <input type="text" placeholder="Enter Advocate name" name="name_of_advocate" id="name_of_advocate" value="<?=$litigant->name_of_advocate?>" class="form-control border-start-0">
                                    <label>अधिवक्ता के नाम</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="form-group mb-3 position-relative check-valid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                                <div class="form-floating">
                                    <input type="text" placeholder="Enter scheduled action" name="scheduled_action" id="scheduled_action" value="<?=$litigant->scheduled_action?>" class="form-control border-start-0">
                                    <label>नियत कार्यवाही</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-theme">अपडेट</button>
                </div>
            </div>
        </form>
        </div>

        <script>
             $("form#litigantForm").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();    
      var formData = new FormData(this);
      formData.append('id',<?=$litigant->id?>)
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
         success: function (data) {
         if(data.status==200) {
            toastr.success(data.message);
            $(':input[type="submit"]').prop('disabled', false);
                   setTimeout(function(){
                  location.href="<?=base_url('litigant')?>";
            }, 1000) 
   
         }else if(data.status==403) {
            toastr.error(data.message);
            $(':input[type="submit"]').prop('disabled', false);
         }else{
            toastr.error('Something went wrong');
            $(':input[type="submit"]').prop('disabled', false);
         }
        },
        error: function(){} 
     });
    });

</script>
