<main class="main mainheight">
<div class="container">
<div class="row">
  <div class="col-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 position-relative ">
    <!-- Grid table-->
    <div class="card border-0 mb-4">
      <div class="card-header">
        <div class="row">
          <div class="col-auto mb-2">
            <i class="bi bi-shop h5 avatar avatar-40 bg-light-theme rounded"></i>
          </div>
          <div class="col align-self-center mb-2">
            <h6 class="d-inline-block mb-0"><?=$page_title?></h6>
          </div>
          <div class="col-12 col-sm-auto mb-2">
            <div class="rounded">
              <a type="button" data-toggle="modal" data-target="#bulkLitigantsModal" class="btn btn-primary" >सामूहिक अपलोड</a>                                        
              <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addVillageModal">Add Village
                </button> -->            
            </div>
          </div>
        </div>
        <div class="row">
        <div class="col-12">
            <form action="<?=base_url('Authantication/searchSessionSet')?>" id="searchForm">
              <div class="input-group">
                <div class="inp_ut">
                  <input type="text" class="form-control" id="disputeNo" name="disputeNo" value="<?=$this->session->userdata('disputeNo')?>" placeholder="कम्प्यूटरीकृत वाद संख्या" autocomplete="off">
                  <span class="s_data" id="coursesData" style="display:none"></span>
                </div>
                <div class="inp_ut">
                  <input type="text" onfocus="(this.type='date')" class="form-control"  name="start_date" id="start_date" value="<?=$this->session->userdata('start_date')?>" onchange="endDateValidate(this.value)" placeholder="दाखिला तिथि से">
                  <!-- <input type="text" onfocus="(this.type='date')" min="<?= date('Y-m-d'); ?>" class="form-control" id="start_date" name="end_date" placeholder="दाखिला तिथि से"> -->
                </div>
                <div class="inp_ut" id="endDate_div">
                  <!-- <input type="text" onfocus="(this.type='date')" min="<?= date('Y-m-d'); ?>" class="form-control" id="start_date" name="end_date" placeholder="दाखिला तिथि तक"> -->
                  <input type="text" class="form-control"  value="<?=$this->session->userdata('end_date')?>" placeholder="दाखिला तिथि तक">
                </div>
                <div class="inp_ut">
                  <select type="text" class="form-control" id="block" name="block" onchange="getVillage(this.value)" placeholder="विकास खण्ड चुनें">
                    <option value=""> विकास खण्ड चुनें</option>
                    <?php foreach($blocks as $block){
                      ?>
                    <option value="<?=$block->id?>" <?=$block->id == $this->session->userdata('block') ? 'selected' : ''?>> <?=$block->name?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="inp_ut">
                  <select type="text" class="form-control" id="village" name="village" placeholder="ग्राम चुनें">
                    <option value=""> ग्राम चुनें</option>
                  </select>
                </div>
                <div class="inp_ut">
                  <button class="btn btn-primary" name="submit" type="submit" >खोजें  <i class="bi bi-search"></i></button>
                </div>
                <div class="inp_ut">
                  <button class="btn btn-danger" name="submit" type="button" onclick="SearchSessionUnset()"><i class="bi bi-bootstrap-reboot"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="card-body p-0">
          <div class="table table-responsive ">
          <table class="table table-striped table-bordered  table-sm" style="width:100%" id="litigantDatatable">
            <thead>
              <tr>
                <th>क्रमांक(<?=$this->session->userdata('village')?>)</th>
                <th nowrap>वाद का प्रकार</th>
                <th nowrap>पुराना वाद संख्या</th>
                <th nowrap>कम्प्यूटरीकृत वाद संख्या</th>
                <th nowrap>अधिनियम धारा</th>
                <th nowrap>ग्राम का नाम</th>
                <th>थाना</th>
                <th nowrap>दाखिला तिथि</th>
                <th nowrap>वादी एवं प्रतिवादी</th>
                <th nowrap>अधिवक्ता के नाम</th>
                <th nowrap>नियत कार्यवाही</th>
                <th nowrap>वाद पर टिप्पड़ी</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($litigants as $key=>$litigant){?>
              <tr>
                <td><?=$key+1?></td>
                <td><?=$litigant->type?></td>
                <td><?=$litigant->old_dispute_no?></td>
                <td><a href="<?=base_url('litigant-view/'.base64_encode($litigant->id))?>" target="_blank"><?=$litigant->dispute_no?></a></td>
                <td><?=$litigant->act_section?></td>
                <td><?=$litigant->village_name?></td>
                <td><?=$litigant->police_station?></td>
                <td><?=$litigant->admission_date?></td>
                <td><?=$litigant->plaintiffs_and_defendants?></td>
                <td><?=$litigant->name_of_advocate?></td>
                <td><?=$litigant->scheduled_action?></td>
                <td nowrap><a href="javascript:void(0)" class="btn btn-primary" onclick="getLitigantModal('<?=$litigant->id?>')" data-toggle="modal" title="वाद पर टिप्पड़ी">वाद पर टिप्पड़ी <i class="bi-file-plus"></i></a> </td>
                <td>
                  </button>
                  <!-- <a data-toggle="modal" data-target="#litigantReportModal"><i class="bi-file-plus"></i></a> -->
                  
                  <a href="<?=base_url('edit-litigant').'/'.base64_encode($litigant->id)?>" data-toggle="tooltip" title="वाद संपादन"><i class="bi bi-pencil-square"></i></a>
                  <a class="text-danger" href="javascript:void(0)" onclick="deleteLitigant(<?=$litigant->id ?>)" title="वाद मिटायें"><i class="bi bi-trash"></i></a>
                </td>
              </tr>
              <?php }?>
            </tbody>
          </table>
          </div>
        </div>
        <div class="card-footer bg-none">
          <div class="row align-items-center text-center">
            <div class=" col-12 mb-2 ">
              <span class=" hide-if-no-paging">
              Showing <span id="footablestot1"></span> page
              </span>
            </div>
            <div class="col-12" id="footable-pagination1"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Bulk Upload Modal start-->
<div class="modal" id="bulkLitigantsModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="<?=base_url('Litigant/bulkStore')?>" method="post" id="litigantExelBulkUploadForm">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">सामूहिक अपलोड</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="mb-2">
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0">File:</span>
                <div class="form-floating">
                  <input type="file" placeholder="Litigant File" name="litigantExel" id="litigantExel" class="form-control border-start-0">
                  <label>फ़ाइल अपलोड करें</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" >जमा करें</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!--Bulk Upload Modal End -->
<!-- Litigant remark Modal start-->
<div class="modal" id="litigantRemarkModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="<?=base_url('Litigant/litigantReport')?>" method="post" id="litigantRemarkForm">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">वाद रिपोर्ट</h4>
          <button type="button" class="close" data-dismiss="modal" onclick="closwModal()">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="mb-2">
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0">टिप्पणी:</span>
                <div class="form-floating">
                  <input type="text" placeholder="टिप्पणी" name="remark" id="remark" class="form-control border-start-0">
                  <label>टिप्पणी</label>
                </div>
              </div>
            </div>
          </div>
          <div class="mb-2">
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0">File:</span>
                <div class="form-floating">
                  <input type="file" placeholder="फ़ाइल अपलोड करें" name="image" id="image" class="form-control border-start-0">
                  <label>फ़ाइल अपलोड करें</label>
                </div>
              </div>
            </div>
          </div>
          <input type="hidden" name="id" id="id" >
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" >जमा करें</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!--Litigant remark Modal End -->
<script type="text/javascript">
  new DataTable('#litigantDatatable');
  
  function getVillageModal(id){
    //alert(id);
    $.ajax({
  url: '<?=base_url('Village/get_village')?>',
  type: 'POST',
  data: {id},
  success: function (data) {
    $('#editVillageModal').modal('show');
    $('#editFormData').html(data);
  }
  });
  }
  
  
  $("form#villageBlockForm").submit(function(e) {
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();    
    var formData = new FormData(this);
    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
       success: function (data) {
       if(data.status==200) {
          toastr.success(data.message);
          $(':input[type="submit"]').prop('disabled', false);
                 setTimeout(function(){
                location.href="<?=base_url('Village')?>";
          }, 1000) 
  
       }else if(data.status==403) {
          toastr.error(data.message);
          $(':input[type="submit"]').prop('disabled', false);
       }else{
          toastr.error('Something went wrong');
          $(':input[type="submit"]').prop('disabled', false);
       }
      },
      error: function(){} 
   });
  });
  
  $("form#editVillageForm").submit(function(e) {
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();    
    var formData = new FormData(this);
    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
       success: function (data) {
       if(data.status==200) {
          toastr.success(data.message);
          $(':input[type="submit"]').prop('disabled', false);
                 setTimeout(function(){
                location.href="<?=base_url('Village')?>";
          }, 1000) 
  
       }else if(data.status==403) {
          toastr.error(data.message);
          $(':input[type="submit"]').prop('disabled', false);
       }else{
          toastr.error('Something went wrong');
          $(':input[type="submit"]').prop('disabled', false);
       }
      },
      error: function(){} 
   });
  });
  
  
  function deleteLitigant(id){
  var messageText  = "आप इस वाद को हटाना चाहते हैं?";
  var confirmText =  'हाँ, इसे बदलो!';
  var message  ="सफलतापूर्वक वाद हट गया!";
  Swal.fire({
  title: 'क्या आपको यकीन है?',
  text: messageText,
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: confirmText
  }).then((result) => {
  if (result.isConfirmed) {
  $.ajax({
  url: '<?=base_url('litigant/delete')?>', 
  method: 'POST',
  data: {id},
  success: function(result){
  toastr.success(message);
  setTimeout(function(){
     window.location.reload();
  }, 2000);
  }
  
  });
  
  }else{
  location.reload();
  }
  })
  } 
  
  $("form#litigantExelBulkUploadForm").submit(function(e) {
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();    
    var formData = new FormData(this);
    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
       success: function (data) {
       if(data.status==200) {
          toastr.success(data.message);
          $(':input[type="submit"]').prop('disabled', false);
                 setTimeout(function(){
                location.href="<?=base_url('Litigant')?>";
          }, 1000) 
  
       }else if(data.status==403) {
          toastr.error(data.message);
          $(':input[type="submit"]').prop('disabled', false);
       }else{
          toastr.error('Something went wrong');
          $(':input[type="submit"]').prop('disabled', false);
       }
      },
      error: function(){} 
   });
  });
  
  $("form#litigantRemarkForm").submit(function(e) {
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();    
    var formData = new FormData(this);
    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
       success: function (data) {
       if(data.status==200) {
          toastr.success(data.message);
          $(':input[type="submit"]').prop('disabled', false);
                 setTimeout(function(){
                location.href="<?=base_url('Litigant')?>";
          }, 1000) 
  
       }else if(data.status==403) {
          toastr.error(data.message);
          $(':input[type="submit"]').prop('disabled', false);
       }else{
          toastr.error('Something went wrong');
          $(':input[type="submit"]').prop('disabled', false);
       }
      },
      error: function(){} 
   });
  });
  
  function getVillage(value){
  $.ajax({
     url: '<?=base_url("Ajax_controller/getVillage")?>',
     type: 'POST',
     data: {value},  
     success: function (data) {
      $('#village').html(data);
     },
   });
  }
  
  $(document).ready(function() {
    getVillage(<?=$this->session->userdata('block')?>);
          });
  
  function endDateValidate(start_date){
        $('#endDate_div').html(' <input type="date" class="form-control" min="'+start_date+'" onchange="get_duration(this.value)" name="end_date" id="end_date" placeholder="दाखिला तिथि तक">');
      }
  
    $("form#searchForm").submit(function(e) {
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();    
    var formData = new FormData(this);
    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
       success: function (data) {
        location.href="<?=base_url('Litigant')?>";
      },
      error: function(){} 
   });
  });
  
  
  function SearchSessionUnset(){
    $.ajax({
     url: '<?=base_url("Authantication/sessionDestroy")?>',
     type: 'POST',
     success: function (data) {
      location.href="<?=base_url('Litigant')?>";
      },
   });
  }
  
  function getLitigantModal(id){
    $('#litigantRemarkModal').modal('show');
     $('#id').val(id);
    }
  
    function closwModal(){
      $('#litigantRemarkModal').modal('hide');
    }
</script>