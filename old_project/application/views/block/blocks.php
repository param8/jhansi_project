<main class="main mainheight">
<div class="container">
  <div class="row">
    <div class="col-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 position-relative ">
      <!-- Grid table-->
      <div class="card border-0 mb-4">
        <div class="card-header">
          <div class="row">
            <div class="col-auto mb-2">
              <i class="bi bi-shop h5 avatar avatar-40 bg-light-theme rounded"></i>
            </div>
            <div class="col align-self-center mb-2">
              <h6 class="d-inline-block mb-0"><?=$page_title?></h6>
            </div>
            <div class="col-12 col-sm-auto mb-2">
              <div class="rounded"> 
                <!-- <a type="button" href="<?=base_url('block-add')?>" class="btn btn-primary" >Add Block</a>-->                               
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addBlockModal">विकास खण्ड जोड़ें 
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped table-bordered" style="width:100%" id="blockDatatable">
            <thead>
              <tr>
                <th>क्रमांक</th>
                <th>नाम</th>
                <th>कार्य</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($blocks as $key=>$block){?>
              <tr>
                <td><?=$key+1?></td>
                <td><?=$block->name?></td>
                <td>
                  <a href="javasvcript:void(0)" onclick="getBlockModal('<?=base64_encode($block->id)?>')" data-toggle="modal" title="विकास खण्ड संपादित करें"><i class="bi bi-pencil-square"></i></a>
                  <a class="text-danger" href="javascript:void(0)" onclick="deleteBlock(<?=$block->id ?>)" title="मिटायें"><i class="bi bi-trash"></i></a>
                </td>
              </tr>
              <?php }?> 
            </tbody>
          </table>
        </div>
        <div class="card-footer bg-none">
          <div class="row align-items-center text-center">
            <div class=" col-12 mb-2 ">
              <span class=" hide-if-no-paging">
              Showing <span id="footablestot1"></span> page
              </span>
            </div>
            <div class="col-12" id="footable-pagination"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Add Modal start-->
<div class="modal" id="addBlockModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="<?=base_url('Block/store')?>" method="post" id="addBlockForm">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">विकास खण्ड जोड़ें</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="mb-2">
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0">नाम:</span>
                <div class="form-floating">
                  <input type="text" placeholder="Address" name="name" id="name" class="form-control border-start-0">
                  <label>विकास खण्ड नाम</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" >जमा करें</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!--Add Modal End -->


<!-- Edit Schoo Modal Start -->
<div class="modal" id="editBlockModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="<?=base_url('Block/update')?>" method="post" id="editBlockForm">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">विकास खण्ड संपादित करें</h4>
          <button type="button" class="close" data-dismiss="modal" onclick="closwModal()" >&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body" id="editFormData">
         
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" >अपडेट</button>
        </div>
      </form>
    </div>
  </div>
</div>
  <!-- Edit School Modal End -->
  
  <script>
   new DataTable('#blockDatatable');


    function getBlockModal(id){
      //alert(id);
      $.ajax({
    url: '<?=base_url('Block/get_block')?>',
    type: 'POST',
    data: {id},
    success: function (data) {
      $('#editBlockModal').modal('show');
      $('#editFormData').html(data);
    }
    });
    }

    function closwModal(){
      $('#editBlockModal').modal('hide');
    }


  $("form#addBlockForm").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();    
      var formData = new FormData(this);
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
         success: function (data) {
         if(data.status==200) {
            toastr.success(data.message);
            $(':input[type="submit"]').prop('disabled', false);
                   setTimeout(function(){
                  location.href="<?=base_url('Block')?>";
            }, 1000) 
   
         }else if(data.status==403) {
            toastr.error(data.message);
            $(':input[type="submit"]').prop('disabled', false);
         }else{
            toastr.error('Something went wrong');
            $(':input[type="submit"]').prop('disabled', false);
         }
        },
        error: function(){} 
     });
    });

    $("form#editBlockForm").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();    
      var formData = new FormData(this);
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
         success: function (data) {
         if(data.status==200) {
            toastr.success(data.message);
            $(':input[type="submit"]').prop('disabled', false);
                   setTimeout(function(){
                  location.href="<?=base_url('Block')?>";
            }, 1000) 
   
         }else if(data.status==403) {
            toastr.error(data.message);
            $(':input[type="submit"]').prop('disabled', false);
         }else{
            toastr.error('Something went wrong');
            $(':input[type="submit"]').prop('disabled', false);
         }
        },
        error: function(){} 
     });
    });


  function deleteBlock(id){
  var messageText  = "आप इस विकास खण्ड को हटाना चाहते हैं?";
  var confirmText =  'हाँ, इसे बदलो!';
  var message  ="विकास खण्ड सफलतापूर्वक हट गया !";
  Swal.fire({
  title: 'क्या आपको यकीन है ?',
  text: messageText,
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: confirmText
  }).then((result) => {
  if (result.isConfirmed) {
  $.ajax({
    url: '<?=base_url('Block/delete')?>', 
    method: 'POST',
    data: {id},
    success: function(result){
    toastr.success(message);
    setTimeout(function(){
       window.location.reload();
    }, 2000);
  }
  
  });
  
  }else{
  location.reload();
  }
  })
  } 
</script>