
    <main class="main mainheight container-fluid">
        <form class="row h-100 justify-content-center" action="<?=base_url('Authantication/login')?>" id="login" method="post">
            <div class="col-12 align-self-center">
                <div class="row align-items-center justify-content-center">
                    <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4 col-xxl-3 text-center text-white">
                        <h3 class="mb-3 mb-lg-4">दाखिल करना</h3>
                        <p class="mb-4">अपनी साख दर्ज करें for<br>दाखिला करने के लिए</p>

                        <div class="mb-4 text-start">

                            <div class="form-group mb-2 position-relative check-valid text-dark">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-text text-theme border-end-0"><i class="bi bi-envelope"></i></span>
                                    <div class="form-floating">
                                        <input type="text" placeholder="Email Address" name="username" id="username" class="form-control border-start-0" autofocus>
                                        <label>मेल पता</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Form elements -->
                            <div class="form-group mb-2 position-relative check-valid text-dark">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-text text-theme border-end-0"><i class="bi bi-key"></i></span>
                                    <div class="form-floating">
                                        <input type="password" placeholder="Enter Password" name="password" id="password" required class="form-control border-start-0" autofocus>
                                        <label for="password">पासवर्ड</label>
                                    </div>
                                    <span class="input-group-text text-secondary  border-end-0" id="viewpassword"><i class="bi bi-eye"></i></span>
                                </div>
                            </div>
							<div class="col-12 col-sm-8 col-md-6 col-lg-8 col-xl-8 col-xxl-8 mt-auto mb-4 text-center d-grid ml-5">
                <!-- submit button -->
                <button class="btn btn-primary btn-lg btn-theme z-index-5 mb-4" type="submit" >लॉगिन <i class="bi bi-arrow-right"></i></button>
                <!-- <p class="text-white"><a href="forgot-password.html" class="text-white">Forgot your password? clicking here</a></p> -->
            </div>
                      </div>
                    </div>
                </div>
            </div>
            <!-- <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4 col-xxl-3 mt-auto mb-4 text-center d-grid"> -->
                <!-- submit button -->
                <!-- <button class="btn btn-lg btn-theme z-index-5 mb-4" type="submit" >Sign in <i class="bi bi-arrow-right"></i></button> -->
                <!-- <p class="text-white"><a href="forgot-password.html" class="text-white">Forgot your password? clicking here</a></p> -->
            <!-- </div> -->
        </form>
    </main>

    <script>
    $("form#login").submit(function(e) {
		
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
        success: function (data) {
        if(data.status==200) {
           toastr.success(data.message);
           $(':input[type="submit"]').prop('disabled', false);
  				setTimeout(function(){
                 location.href="<?=base_url('dashboard')?>";
           }, 1000) 
  
        }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
        }else{
           toastr.error('Something went wrong');
           $(':input[type="submit"]').prop('disabled', false);
        }
       },
       error: function(){} 
    });
   });
    </script>


	<!-- Vendor JS -->
	

