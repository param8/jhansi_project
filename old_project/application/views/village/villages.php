<main class="main mainheight">
<div class="container">
  <div class="row">
    <div class="col-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 position-relative ">
      <!-- Grid table-->
      <div class="card border-0 mb-4">
        <div class="card-header">
          <div class="row">
            <div class="col-auto mb-2">
              <i class="bi bi-shop h5 avatar avatar-40 bg-light-theme rounded"></i>
            </div>
            <div class="col align-self-center mb-2">
              <h6 class="d-inline-block mb-0"><?=$page_title?></h6>
            </div>
            <div class="col-12 col-sm-auto mb-2">
              <div class="rounded">
                <!-- <a type="button" data-toggle="modal" data-target="#bulkVillageModal" class="btn btn-primary" >Bulk Upload Village</a>                                         -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addVillageModal">गांव जोड़ें
                </button>
              </div>
            </div>
            <!-- <div class="col-12 ">
              <div class="input-group ">
                <span class="input-group-text text-theme"><i class="bi bi-search"></i></span>
                <input type="text" class="form-control" onkeyup="villageSearch(this.value)" placeholder="Search..."></a>
              </div>
            </div> -->
          </div>
        </div>
        <div class="card-body p-0">
        <table id="villageDatatable" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>क्रमांक</th>
                <th>नाम</th>
                <th>नाम</th>
                <th>कार्य</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($villages as $key=>$village){?>
            <tr>
                <td><?=$key+1?></td>
                <td><?=$village->name?></td>
                <td><?=$village->blockName?></td>
                <td>
                     <!-- <a href="#" onclick="getBlockModal(<?=base64_encode($village->id) ?>)" data-toggle="tooltip" title="Edit Block"><i class="bi bi-pencil-square"></i></a> -->
                     <a href="#" onclick="getVillageModal('<?=base64_encode($village->id)?>')" data-toggle="modal" title="गाँव संपादित करें"><i class="bi bi-pencil-square"></i></a>
                     <a class="text-danger" href="javascript:void(0)" onclick="deleteVillage(<?=$village->id ?>)" title="मिटायें"><i class="bi bi-trash"></i></a>
                </td>
            </tr>
           <?php } ?>
        </tbody>
    </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Add Modal start-->
<div class="modal" id="addVillageModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="<?=base_url('Village/store')?>" method="post" id="villageBlockForm">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">गांव जोड़ें</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="mb-2">
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0">नाम:</span>
                <div class="form-floating">
                  <input type="text" placeholder="Village Name" name="name" id="name" class="form-control border-start-0">
                  <label>गाँव का नाम</label>
                </div>
              </div>
            </div>
          </div>
          <div class="mb-2">
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0">विकास खण्ड:</span>
                <div class="form-floating">
                  <select name="villageBlock" id="villageBlock" class="form-control border-start-0">
                  <option value="">विकास खण्ड चुनें</option>
               <?php foreach($blocks as $block){ ?>
                  <option value="<?=$block->id?>"><?=$block->name?></option>
                  <?php } ?>
              </select>
                  <label>विकास खण्ड</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" >जमा करें</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!--Add Modal End -->


<!-- Edit Schoo Modal Start -->
<div class="modal" id="editVillageModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="<?=base_url('Village/update')?>" method="post" id="villageEditBlockForm">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">गाँव संपादित करें</h4>
          <button type="button" class="close" data-dismiss="modal" onclick="closwModal()">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body" id="editFormData">
         
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" >अपडेट</button>
        </div>
      </form>
    </div>
  </div>
</div>
  <!-- Edit School Modal End -->

  <!-- Bulk Upload Modal start-->
<div class="modal" id="bulkVillageModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="<?=base_url('Village/bulkStore')?>" method="post" id="litigantExelBulkUploadForm">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">सामूहिक ग्राम डेटा अपलोड करें</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="mb-2">
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0">फ़ाइल:</span>
                <div class="form-floating">
                  <input type="file" placeholder="Village File" name="litigantExel" id="litigantExel" class="form-control border-start-0">
                  <label>फ़ाइल अपलोड करें</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" >जमा करें</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!--Bulk Upload Modal End -->
  
  <script>


$(document).ready(function() {
  new DataTable('#villageDatatable');
  
       var dataTable = $('#villageDatatable').DataTable({
           "processing": true,
           "serverSide": true,
           buttons: [{
               extend: 'excelHtml5',
               text: 'Download Excel'
           }],
           "order": [],
           "ajax": {
               url: "<?=base_url('Village/ajaxVillage')?>",
               type: "POST"
           },
           "columnDefs": [{
               "targets": [0],
               "orderable": false,
           }, ],
       });
   });
    
    function getVillageModal(id){
      //alert(id);
      $.ajax({
    url: '<?=base_url('Village/get_village')?>',
    type: 'POST',
    data: {id},
    success: function (data) {
      $('#editVillageModal').modal('show');
      $('#editFormData').html(data);
    }
    });
    }

    function closwModal(){
      $('#editVillageModal').modal('hide');
    }

  $("form#villageBlockForm").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();    
      var formData = new FormData(this);
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
         success: function (data) {
         if(data.status==200) {
            toastr.success(data.message);
            $(':input[type="submit"]').prop('disabled', false);
                   setTimeout(function(){
                  location.href="<?=base_url('Village')?>";
            }, 1000) 
   
         }else if(data.status==403) {
            toastr.error(data.message);
            $(':input[type="submit"]').prop('disabled', false);
         }else{
            toastr.error('Something went wrong');
            $(':input[type="submit"]').prop('disabled', false);
         }
        },
        error: function(){} 
     });
    });

    $("form#villageEditBlockForm").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();    
      var formData = new FormData(this);
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
         success: function (data) {
         if(data.status==200) {
            toastr.success(data.message);
            $(':input[type="submit"]').prop('disabled', false);
                   setTimeout(function(){
                  location.href="<?=base_url('Village')?>";
            }, 1000) 
   
         }else if(data.status==403) {
            toastr.error(data.message);
            $(':input[type="submit"]').prop('disabled', false);
         }else{
            toastr.error('Something went wrong');
            $(':input[type="submit"]').prop('disabled', false);
         }
        },
        error: function(){} 
     });
    });


  function deleteVillage(id){
  var messageText  = "आप इस गांव को हटाना चाहते हैं?";
  var confirmText =  'हाँ, इसे बदलो!';
  var message  ="ग्राम सफलतापूर्वक हटाएँ!";
  Swal.fire({
  title: 'क्या आपको यकीन है?',
  text: messageText,
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: confirmText
  }).then((result) => {
  if (result.isConfirmed) {
  $.ajax({
    url: '<?=base_url('Village/delete')?>', 
    method: 'POST',
    data: {id},
    success: function(result){
    toastr.success(message);
    setTimeout(function(){
       window.location.reload();
    }, 2000);
  }
  
  });
  
  }else{
  location.reload();
  }
  })
  } 

  $("form#litigantExelBulkUploadForm").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();    
      var formData = new FormData(this);
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
         success: function (data) {
         if(data.status==200) {
            toastr.success(data.message);
            $(':input[type="submit"]').prop('disabled', false);
                   setTimeout(function(){
                  location.href="<?=base_url('Village')?>";
            }, 1000) 
   
         }else if(data.status==403) {
            toastr.error(data.message);
            $(':input[type="submit"]').prop('disabled', false);
         }else{
            toastr.error('Something went wrong');
            $(':input[type="submit"]').prop('disabled', false);
         }
        },
        error: function(){} 
     });
    });
</script>