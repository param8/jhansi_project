<?php 

// class MY_Controller extends CI_Controller
// {
// 	public function __construct()
// 	{
// 		parent::__construct();
	
// 	}
// }

class MY_Controller extends CI_Controller 
{
	var $permission = array();

	public function __construct() 
	{
	   parent::__construct();
 
        $this->load->model('Common_model');
		$this->load->helper('mail');
		if(empty($this->session->userdata('logged_in'))) {
			$session_data = array('logged_in' => FALSE);
			$this->session->set_userdata($session_data);
		}
	}

	public function logged_in()
	{
		$session_data = $this->session->userdata();
		if($session_data['logged_in'] == TRUE) {
			if($session_data['user_type'] == 'Admin' || $session_data['user_type'] == 'User')
			{
				redirect('dashboard', 'refresh');	
			}else{
				redirect('login', 'refresh');
			}	
		}
	}

	public function not_logged_in()
	{
		$session_data = $this->session->userdata();
		if($session_data['logged_in'] == FALSE) {
			redirect('home', 'refresh');
		}
	}

	public function not_admin_logged_in()
	{
		$session_data = $this->session->userdata();
		// print_r($session_data);die;
		// if($session_data['logged_in'] == FALSE) {
		// 	redirect('login', 'refresh');
		// }
	}


	 public function stateinfo(){
		$stateinfo = $this->Common_model->get_states();
		return $stateinfo;
	 }

	 public function cityinfo(){
		$cityinfo = $this->Common_model->get_cities();
		return $cityinfo;
	 }

	

	 public function admin_template($page = null, $data = array())
	 {
		$data['states'] = $this->stateinfo();
		$this->load->view('layout/head',$data);
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view($page);
		$this->load->view('layout/footer');
	 }


}