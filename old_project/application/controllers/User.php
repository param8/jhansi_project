<?php 
class User extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_admin_logged_in();
		$this->load->model('user_model');
	}

	public function index()
	{
    $data['page_title'] = 'उपयोगकर्ता विवरण';
    $data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'User'));
		$this->admin_template('user/users',$data);
	}

	public function add(){
    $data['page_title'] = 'उपयोगकर्ता जोड़ें';
		$this->admin_template('user/addUser',$data);
	}
	public function edit(){
	$id = base64_decode($this->uri->segment(2));
    $data['page_title'] = 'उपयोगकर्ता को संपादित करो';
    $data['user'] = $this->user_model->get_user(array('id' => $id));
		$this->admin_template('user/editUser',$data);
	}

   public function store(){
	$firstName = $this->input->post('firstName');
	// $lastName = $this->input->post('lastName');
	$email = $this->input->post('email');
	$password = $this->input->post('password');
	$contact = $this->input->post('contact');
	$userType = $this->input->post('userType');

	if(empty($firstName)){
		echo json_encode(['status'=>403, 'message'=>'कृपया अपना नाम दर्ज करें']); 	
		exit();
	}
	// if(empty($lastName)){
	// 	echo json_encode(['status'=>403, 'message'=>'Please enter your last name']); 	
	// 	exit();
	// }
	if(empty($email)){
		echo json_encode(['status'=>403, 'message'=>'कृपया अपना ईमेल ईमेल दर्ज करें']); 	
		exit();
	}
	$checkEmail = $this->user_model->get_user(array('email'=>$email));
	if($checkEmail){
		echo json_encode(['status'=>403,'message'=>'यह ईमेल पहले से प्रयोग में है']);
		exit();
	}
	if(empty($password)){
		echo json_encode(['status'=>403, 'message'=>'अपना पासवर्ड दर्ज करें']); 	
		exit();
	}
	// if(empty($contact)){
	// 	echo json_encode(['status'=>403, 'message'=>'Please enter your mobile']); 	
	// 	exit();
	// }
 
	if(empty($userType)){
		echo json_encode(['status'=>403, 'message'=>'कृपया उपयोगकर्ता प्रकार चुनें']); 	
		exit();
	}

	$this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/user',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/user/'.$config['file_name'].'.'.$type;
      }
     }else{
      $image = 'public/dummy_image.jpg';
    }

	$data = array(
		'name'        => $firstName,
		'last_name'   => $lastName,
		'email'       => $email,
		'profile_pic' => $image,
		'password'    => md5($password),
		'contact'     => $contact,
		'address'     => $address,
		// 'user_type'   => 'user',
	);
	$register = $this->user_model->store($data);

	if($register){
		echo json_encode(['status'=>200, 'message'=>'उपयोगकर्ता सफलतापूर्वक पंजीकृत हुआ!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'कुछ ग़लत हुआ']);   
	}

   }

	public function update_status()
	{
		$user_id = $this->input->post('id');
		$user_status = ($this->input->post('status')=='1') ?'0':'1';

        $data =  array(
          'status' => $user_status
        );
	
        $update_status =  $this->user_model->update($data,array('id'=>$user_id));
   
	}


  public function update(){
    $id = $this->input->post('id');
	$status = $this->input->post('status');
	$name = $this->input->post('firstName');
	// $lastName = $this->input->post('lastName');
	$email = $this->input->post('email');
	$contact = $this->input->post('contact');
	$userType = $this->input->post('userType');

	$user = $this->user_model->get_user(array('users.id'=>$id));
	if(empty($name)){
		echo json_encode(['status'=>403, 'message'=>'कृपया अपना नाम दर्ज करें']); 	
		exit();
	}
	// if(empty($lastName)){
	// 	echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
	// 	exit();
	// }
	if(empty($email)){
		echo json_encode(['status'=>403, 'message'=>'कृपया अपना ईमेल एड्रेस इंटर करें']); 	
		exit();
	}
	// $checkEmail = $this->user_model->get_user(array('email'=>$email));
	// if($checkEmail){
	// 	echo json_encode(['status'=>403,'message'=>'This email is already in use']);
	// 	exit();
	// }
	if(empty($contact)){
		echo json_encode(['status'=>403, 'message'=>'कृपया अपना फोन नंबर दर्ज करें']); 	
		exit();
	}
	if(empty($userType)){
		echo json_encode(['status'=>403, 'message'=>'कृपया उपयोगकर्ता प्रकार चुनें']); 	
		exit();
	}

	$this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/user',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/user/'.$config['file_name'].'.'.$type;
      }
     }elseif(!empty($user->profile_pic)){
		$image = $user->profile_pic;
    }else{
        $image = 'public/dummy_image.jpg';
	}

	$data = array(
		'name'        => $name,
		'email'       => $email,
		'contact'     => $contact,
		'profile_pic' => $image,
		'userType'    => $userType,
		'status'      => $status,
	);
	$update = $this->user_model->update($data,array('id'=>$id));

	if($update){
		echo json_encode(['status'=>200, 'message'=>'उपयोगकर्ता अद्यतन सफलतापूर्वक!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'कुछ ग़लत हुआ']);   
	}
  }

	public function viewUser(){
		$userid = $this->input->post('userid');
		$user = $this->user_model->get_user_details(array('users.id'=>$userid));
		?>
			<div class="box mb-0">
				<div class="box-body box-profile">            
					<div class="row">
						<div class="col-12">
							<h4 class="box-inverse p-2">Personal Information</h4>
							<div>
								<p><strong>Name</strong> :<span class="text-gray pl-10"><?=$user->name;?></span> </p>
								<p><strong>Email</strong> :<span class="text-gray pl-10"><?=$user->email;?></span> </p>
								<p><strong>Phone</strong> :<span class="text-gray pl-10"><?=$user->contact;?></span></p>
								<p><strong>Address</strong> :<span class="text-gray pl-10"><?=$user->address;?> </span></p>
								<p><strong>State</strong> :<span class="text-gray pl-10"><?=$user->stateName;?></span></p>
								<p><strong>City</strong> :<span class="text-gray pl-10"><?=$user->cityName;?></span></p>
							</div>
							
							
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		<?php
	  }

	  public function delete(){
		$id = $this->input->post('id');
		$delete = $this->user_model->delete(array('id'=>$id));
		if($delete){
			echo json_encode(['status'=>200, 'message'=>'उपयोगकर्ता सफलतापूर्वक हटाएँ!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'कुछ ग़लत हुआ']);   
		}
	  }

	
}