<?php 
class Block extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		//$this->load->model('user_model');
        $this->load->model('block_model');
	}

    public function index(){
    $data['page_title'] = 'विकास खण्ड';
    $data['blocks'] = $this->block_model->get_all_blocks(array('status' => 1));
    //print_r($data['blocks']);
		$this->admin_template('block/blocks',$data);
	}

    public function ajaxBlock(){
        $this->not_admin_logged_in();
		$condition = array('status'=>1);
		$blocks = $this->block_model->make_datatables($condition); // this will call modal function for fetching data
        $data = array();
		foreach($blocks as $key=>$block) // Loop over the data fetched and store them in array
		{
            $button = '';
			$sub_array = array();
			$button .= '<a href="'.base_url('view-course/'.base64_encode($block['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View faculty" class="btn  btn-sm  bg-warning-light"><i class="fe fe-eye"></i></a>';
			$button .= '<a href="'.base_url('edit-course/'.base64_encode($block['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit faculty" class="btn btn-sm bg-success-light"><i class="fe fe-edit"></i> </a>';
            $button .= '<a href="javascript:void(0)" onclick="delete_course('.$block['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete faculty" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';	
			$sub_array[] = $key+1;
			$sub_array[] = $block['name'];
			$sub_array[] = date('d-F-Y', strtotime($block['created_at']));
			$sub_array[] = $button;
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->block_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->block_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
    }

    public function store(){
        $name = $this->input->post('name');

        if(empty($name)){
            echo json_encode(['status'=>403, 'message'=>'Please enter Block name']); 	
            exit();
        }

        $data = array(
            'name'        => $name,
        );
        $register = $this->block_model->store($data);
    
        if($register){
            echo json_encode(['status'=>200, 'message'=>'Block Add successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
        }
    
        
    }

    public function get_block(){
        $id = base64_decode($this->input->post('id')); 
        $blocks = $this->block_model->get_blocks(array('id' => $id));
        ?>
        <div class="form-group">
        <label for="name" class="col-form-label">Block:</label>
        <input type="text" placeholder="Address" name="name" id="name" value="<?=$blocks->name?>" class="form-control border-start-0">
      </div>
   
      <input type="hidden" name="id" id="id" value="<?=$id?>">
   
   <?php }

public function update(){
    $id = $this->input->post('id');
    $name = $this->input->post('name');

    if(empty($name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter Block name']); 	
        exit();
    }

    $data = array(
        'name'        => $name,
    );
    $update = $this->block_model->update($data, $id);

    if($update){
        echo json_encode(['status'=>200, 'message'=>'Block Update successfully!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
    }  
}

public function delete(){
    $id = $this->input->post('id'); 
    $delete = $this->block_model->delete($id);

    if($delete){
        echo json_encode(['status'=>200, 'message'=>'Block Delete successfully!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
    }  
}
}