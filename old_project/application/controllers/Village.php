<?php 
class Village extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('village_model');
        $this->load->model('block_model');
        $this->load->library('Csvimport');
	}

    public function index(){
        $data['page_title'] = 'ग्राम';
         $data['blocks'] = $this->block_model->get_all_blocks(array('status' => 1));
         $data['villages'] = $this->village_model->get_all_villages(array('village.status' => 1));
        $this->admin_template('village/villages',$data);
        }

        public function store(){
            $name = $this->input->post('name');
            $blockId = $this->input->post('villageBlock');
    
           if(empty($name)){
                echo json_encode(['status'=>403, 'message'=>'कृपया गांव का नाम दर्ज करें']); 	
                exit();
            } 
            if(empty($blockId)){
                echo json_encode(['status'=>403, 'message'=>'कृपया विकास खण्ड चुनें']); 	
                exit();
            }   
            $data = array(
                'name'        => $name,
                'blockID'     => $blockId,
            );
            $register = $this->village_model->store($data);
        
            if($register){
                echo json_encode(['status'=>200, 'message'=>'ग्राम सफलतापूर्वक जोड़ें!']);
            }else{
                echo json_encode(['status'=>302, 'message'=>'कुछ ग़लत हुआ']);   
            } 
        }

        public function get_village(){
            $id = base64_decode($this->input->post('id')); 
            $village = $this->village_model->get_village(array('id' => $id));
            $blocks = $this->block_model->get_all_blocks(array('status' => 1));
            ?>
            <div class="mb-2">
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0">नाम:</span>
                <div class="form-floating">
                  <input type="text" placeholder="Village Name" name="name" value="<?=$village->name?>" id="name" class="form-control border-start-0">
                  <label>ग्राम नाम</label>
                </div>
              </div>
            </div>
          </div>
          <div class="mb-2">
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0">विकास खण्ड:</span>
                <div class="form-floating">
                  <select name="villageBlock" id="villageBlock" class="form-control border-start-0">
                  <option value="">विकास खण्ड चुनें</option>
               <?php foreach($blocks as $block){ ?>
                  <option value="<?=$block->id?>" <?=$block->id == $village->blockID ? 'selected' : ''?>><?=$block->name?></option>
                  <?php } ?>
              </select>
                  <label>विकास खण्ड</label>
                </div>
              </div>
            </div>
          </div>
       
          <input type="hidden" name="id" id="id" value="<?=$id?>">
       
       <?php }

  public function update(){
    $id = $this->input->post('id');
    $name = $this->input->post('name');
    $villageBlockId = $this->input->post('villageBlock');

    if(empty($name)){
        echo json_encode(['status'=>403, 'message'=>'कृपया गांव का नाम दर्ज करें']); 	
        exit();
    }
    if(empty($villageBlockId)){
        echo json_encode(['status'=>403, 'message'=>'कृपया विकास खण्ड चुनें']); 	
        exit();
    }
    $data = array(
        'name'        => $name,
        'blockID'     => $villageBlockId,
    );
    $blockupdate = $this->village_model->update($data,$id);
    if($blockupdate){
        echo json_encode(['status'=>200, 'message'=>'ग्राम अद्यतन सफलतापूर्वक!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'कुछ ग़लत हुआ']);   
    }  
}

public function delete(){
    $id = $this->input->post('id'); 
    $delete = $this->village_model->delete($id);

    if($delete){
        echo json_encode(['status'=>200, 'message'=>'ग्राम सफलतापूर्वक हटाएँ!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'कुछ ग़लत हुआ']);   
    }  
}

// function bulkStore(){
//     if (isset($_FILES["litigantExel"])) {
//         //  echo "hi";die;
//         $config['upload_path']   = "uploads/csv/village";
//         $config['allowed_types'] = 'text/plain|text/csv|csv';
//         $config['max_size']      = '2048';
//         $config['file_name']     = $_FILES["litigantExel"]['name'];
//         $config['overwrite']     = TRUE;
//         $this->load->library('upload', $config);
//         $this->upload->initialize($config);
//         //echo "hi";
//         if (!$this->upload->do_upload("litigantExel")) {
//         echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
//         }else{
//         $file_data = $this->upload->data();
//         //print_r($file_data);die;
//         $file_path = 'uploads/csv/village/'.$file_data['file_name'];
//         if ($this->csvimport->get_array($file_path)) {
//             $csv_array = $this->csvimport->get_array($file_path);  
             
//             foreach($csv_array as $key=>$row){
//               $data = array(
//                 'type'                      => $row['dispute_no'],
//                 'old_dispute_no'            => $row['old_dispute_no'],
//                 'dispute_no'                => $row['dispute_no'],
//                 'act_section'               => $row['act_section'],
//                 'village_name'              => $row['village_name'],
//                 'police_station'            => $row['police_station'],
//                 'plaintiffs_and_defendants' => $row['plaintiffs_and_defendants'],
//                 'name_of_advocate'          => $row['name_of_advocate'],
//                 'scheduled_action'          => $row['scheduled_action'],
//               );
//             }
//               $store = $this->village_model->store_litigants($data);
//               if($store){
//                 echo json_encode(['status'=>200, 'message'=>'litigant Exel Upload successfully!']);
//             }else{
//                 echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
//             }  
            
//         } 
//       }
//      }
//     }


public function ajaxVillage(){
    $this->not_admin_logged_in();
    $condition = array('village.status'=>1);
    $villages = $this->village_model->make_datatables($condition); // this will call modal function for fetching data
    $data = array();
    foreach($villages as $key=>$village) // Loop over the data fetched and store them in array
    {
        $button = '';
        $sub_array = array();
        $button .= '<a href="'.base_url('edit-course/'.base64_encode($course['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit faculty" class="btn btn-sm bg-success-light"><i class="fe fe-edit"></i> </a>';
        $button .= '<a href="javascript:void(0)" onclick="delete_course('.$course['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete faculty" class="btn btn-sm bg-danger-light"><i class="fe fe-trash"></i> </a>';
        $sub_array[] = $key+1;
        $sub_array[] = $village['name'];
        $sub_array[] = $village['blockName'];
        $sub_array[] = $button;
      $data[] = $sub_array;
    }
    $output = array(
        "draw"                    =>     intval($_POST["draw"]),
        "recordsTotal"            =>     $this->village_model->get_all_data($condition),
        "recordsFiltered"         =>     $this->village_model->get_filtered_data($condition),
        "data"                    =>     $data
    );
    
    echo json_encode($output);
  }


}