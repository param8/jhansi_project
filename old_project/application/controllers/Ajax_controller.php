<?php 
class Ajax_controller extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
    //$this->not_admin_logged_in();

	}

	public function getVillage()
    {
       $blockID =  $this->input->post('value');
       $villages = $this->Common_model->get_village_block_wise($blockID);
       //print_r($villages);
       if(count($villages) > 0)
       {
        ?>
        <option value="">ग्राम चुनें</option>
        <?php foreach($villages as $village){ 
         $villageId = $this->session->userdata('village');
         ?>
           <option value="<?=$village->name?>" <?=$village->name == $villageId ? 'selected' : ''?>><?=$village->name?></option>
        <?php
        }
       }else
       {
        ?>
        <option value="">ग्राम नहीं है !</option>
        <?php
       }
    }
}