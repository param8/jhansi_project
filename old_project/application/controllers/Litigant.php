<?php 
class Litigant extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('village_model');
        $this->load->model('litigant_model');
        $this->load->model('block_model');
        $this->load->library('Csvimport');
	}

    public function index(){
        $data['page_title'] = 'वाद सूची';
        $data['litigants'] = $this->litigant_model->get_all_litigants(array('status' => 1));
        $data['blocks'] = $this->block_model->get_all_blocks(array('status' => 1));
        //print_r($data['litigants']);
            $this->admin_template('litigant/litigants',$data);
        }

    public function edit(){
        $data['page_title'] = 'एडिट वाद सूची';
        $id = base64_decode($this->uri->segment(2));
        $data['litigant'] = $this->litigant_model->get_litigant(array('id' => $id)); 
        $this->admin_template('litigant/edit_litigant',$data);   
    }

    public function litigant_view(){
        $id = base64_decode($this->uri->segment(2));
        $data['page_title'] = 'वाद का सारांश';
        $data['litigant'] = $this->litigant_model->get_litigant(array('id' => $id)); 
        $this->admin_template('litigant/litigant_view',$data);   
    }

    

    function bulkStore(){
        if (isset($_FILES["litigantExel"])) {
            //  echo "hi";die;
            $config['upload_path']   = "uploads/csv/village";
            $config['allowed_types'] = 'text/plain|text/csv|csv';
            $config['max_size']      = '2048';
            $config['file_name']     = $_FILES["litigantExel"]['name'];
            $config['overwrite']     = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            //echo "hi";
            if (!$this->upload->do_upload("litigantExel")) {
            echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
            }else{
            $file_data = $this->upload->data();
            //print_r($file_data);die;
            $file_path = 'uploads/csv/village/'.$file_data['file_name'];
            if ($this->csvimport->get_array($file_path)) {
                $csv_array = $this->csvimport->get_array($file_path);  
                 
                foreach($csv_array as $key=>$row){
                  $data = array(
                    'type'                      => $row['type'],
                    'old_dispute_no'            => $row['old_dispute_no'],
                    'dispute_no'                => $row['dispute_no'],
                    'act_section'               => $row['act_section'],
                    'village_name'              => $row['village_name'],
                    'police_station'            => $row['police_station'],
                    'admission_date'            => $row['admission_date'],
                    'plaintiffs_and_defendants' => $row['plaintiffs_and_defendants'],
                    'name_of_advocate'          => $row['name_of_advocate'],
                    'scheduled_action'          => $row['scheduled_action'],
                  );
                  $store = $this->litigant_model->store($data);
            }
                if($store){
                    echo json_encode(['status'=>200, 'message'=>'litigant Exel Upload successfully!']);
                }else{
                    echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
                }  
            } 
          }
         }
        }
    

        public function update(){
            $id = $this->input->post('id');
            $type = $this->input->post('type');
            $old_dispute_no = $this->input->post('old_dispute_no');
            $dispute_no = $this->input->post('dispute_no');
            $act_section = $this->input->post('act_section');
            $village_name = $this->input->post('village_name');
            $police_station = $this->input->post('police_station');
            $admission_date = $this->input->post('admission_date');
            $plaintiffs_and_defendants = $this->input->post('plaintiffs_and_defendants');
            $name_of_advocate = $this->input->post('name_of_advocate');
            $scheduled_action = $this->input->post('scheduled_action');

            if(empty($type)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या वाद का प्रकार लिखें !']); 	
                exit();
            }
            if(empty($old_dispute_no)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या पुराना वाद संख्या लिखें !']); 	
                exit();
            }
            if(empty($dispute_no)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या कम्प्यूटरीकृत वाद संख्या लिखें !']); 	
                exit();
            }
            if(empty($act_section)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या अधिनियम धारा लिखें !']); 	
                exit();
            }
            if(empty($village_name)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या ग्राम का नाम लिखें !']); 	
                exit();
            }
            if(empty($police_station)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या थाना का नाम लिखें !']); 	
                exit();
            }
            if(empty($admission_date)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या दाखिला तिथि चुनें !']); 	
                exit();
            }
            if(empty($plaintiffs_and_defendants)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या वादी एवं प्रतिवादी लिखें !']); 	
                exit();
            }
            if(empty($name_of_advocate)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या अधिवक्ता के नाम लिखें !']); 	
                exit();
            }
            if(empty($scheduled_action)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या नियत कार्यवाही लिखें !']); 	
                exit();
            }

            $data = array(
                'type' => $type,
                'old_dispute_no' => $old_dispute_no,
                'dispute_no' => $dispute_no,
                'act_section' => $act_section,
                'village_name' => $village_name,
                'police_station' => $police_station,
                'admission_date' => $admission_date,
                'plaintiffs_and_defendants' => $plaintiffs_and_defendants,
                'name_of_advocate' => $name_of_advocate,
                'scheduled_action' => $scheduled_action,
            );

            $update = $this->litigant_model->update($data, $id);
            if($update){
              echo json_encode(['status'=>200, 'message'=>'Litigant Update successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}
    }

    public function litigantReport(){
     $id     = $this->input->post('id');
     $remark = $this->input->post('remark');
     
     if(empty($remark)){
        echo json_encode(['status'=>403, 'message'=>'कृप्या टिप्पणी दें!']);
        exit();
     }

    $this->load->library('upload');
    if($_FILES['image']['name']!= '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/remark',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp|pdf|txt',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/remark/'.$config['file_name'].'.'.$type;
      }
     }else{
        $image = 'public/dummy_image.jpg';
	}

	$data = array(
        'litigantId'    => $id,
		'remark'        => $remark,
		'file'          => $image,
	);
	$store = $this->litigant_model->store_remark($data);

	if($store){
		echo json_encode(['status'=>200, 'message'=>'सफलतापूर्वक टिप्पणी जमा !']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}
    }

    public function delete(){
		$id = $this->input->post('id');
		$delete = $this->litigant_model->delete($id);
		if($delete){
			echo json_encode(['status'=>200, 'message'=>'Litigant_model Delete successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
		}
	  }

}
