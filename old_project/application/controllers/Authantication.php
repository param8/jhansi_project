<?php 
class Authantication extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		//$this->not_admin_logged_in();
		$this->load->model('Auth_model');
		$this->load->model('User_model');
        $this->load->model('setting_model');
	}


	public function index()
	{	
		$data['page_title'] = 'Login';
		$this->load->view('layout/login_head',$data);
		$this->load->view('login');
		$this->load->view('layout/login_footer');

	}

	public function login(){

		 $email = $this->input->post('username');
		$password = $this->input->post('password');
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email or contact']); 	
			exit();
		}
		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
			exit();
		}
		$login = $this->Auth_model->login($email,$password);
		if($login==403){
			echo json_encode(['status'=>403, 'message'=>'email or password incorrect!']);
		}elseif($login==302){
			echo json_encode(['status'=>403, 'message'=>'Your are not active please contact the administrator']);   
		}else{
      echo json_encode(['status'=>200, 'message'=>'Login Successfully','user_type'=>$this->session->userdata('user_type')]);
    }
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url(), 'refresh');
	}

//   Admin Cradinciales


public function adminLogin(){
	//$this->logged_in();
	 $email = $this->input->post('username');
	 $password = $this->input->post('password');
	 $type = 'Admin';
	if(empty($email)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
		exit();
	}
	if(empty($password)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
		exit();
	}
	$login = $this->Auth_model->login($email,$password);

	if($login){
		echo json_encode(['status'=>200, 'message'=>'Login successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'Invalid username or password!']);   
	}
}

public function adminLogout()
{
	$this->session->sess_destroy();
	redirect(base_url(), 'refresh');
}

public function searchSessionSet(){
	$disputeNo = $this->input->post('disputeNo');
	$start_date = $this->input->post('start_date');
	$end_date = $this->input->post('end_date');
	$block = $this->input->post('block');
	$village = $this->input->post('village');
	
	$session = array(
		'disputeNo' => $disputeNo,
		'start_date' => $start_date,
		'end_date' => $end_date,
		'block' => $block,
		'village' => $village,
	);

	$this->session->set_userdata($session);
}
	public function sessionDestroy(){
	$this->session->unset_userdata('disputeNo');
	$this->session->unset_userdata('start_date');
	$this->session->unset_userdata('end_date');
	$this->session->unset_userdata('block');
	$this->session->unset_userdata('village');
	}
	
}