<main class="main mainheight">
        <div class="container">
            <h5 class="title"><?=$page_title?></h5>
            <div class="row justify-content-center">
                <div class="col-12 col-lg-7 col-xl-8 mb-4">
                <div class="card p-5">
                    <h6 class="title">मूल जानकारी</h6>
                    <form action="<?=base_url('User/store')?>" id="addForm" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="mb-2">
                            <div class="form-group mb-3 position-relative check-valid">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-text text-theme border-end-0"><i class="bi bi-person"></i></span>
                                    <div class="form-floating">
                                        <input type="text" placeholder="Enter Name" name="firstName" id="firstName" class="form-control border-start-0">
                                        <label>नाम</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-2">
                        <div class="form-group mb-3 position-relative check-valid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class="bi bi-envelope"></i></span>
                                <div class="form-floating">
                                    <input type="text" placeholder="Email address" name="email" id="email" class="form-control border-start-0">
                                    <label>मेल पता</label>
                                </div>
                            </div>
                        </div>
                    </div>

                 
                    <div class="form-group mb-2 position-relative check-valid text-dark">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-text text-theme border-end-0"><i class="bi bi-key"></i></span>
                                    <div class="form-floating">
                                        <input type="password" placeholder="Enter Password" name="password" id="password" class="form-control border-start-0" autofocus>
                                        <label for="password">पासवर्ड</label>
                                    </div>
                                    <span class="input-group-text text-secondary  border-end-0" id="viewpassword"><i class="bi bi-eye"></i></span>
                                </div>
                            </div>
                    
                    <div class="mb-2">
                        <div class="form-group mb-3 position-relative check-valid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class="bi bi-phone"></i></span>
                                <div class="form-floating">
                                    <input type="text" placeholder="" name="contact" id="contact" class="form-control border-start-0" maxlength="10" minlength="10"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                    <label>फ़ोन नंबर</label>
                                </div>
                            </div>
                        </div>
                    </div>
            
                    <div class="mb-2">
                        <div class="form-group mb-3 position-relative check-valid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class="bi bi-user"></i></span>
                                <div class="form-floating">
                                    <select name="userType" id="userType" class="form-control border-start-0"> 
                                        <option value="">प्रकार चुनें</option>
                                        <option value="व्यवस्थापक">व्यवस्थापक</option>
                                        <option value="उपयोगकर्ता">उपयोगकर्ता</option>
                                    </select>
                                    <label>उपयोगकर्ता का प्रकार</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-theme">जमा करना</button>
                </form>
</div>
                </div>
            </div>
        </div>

        <script>
    $("form#addForm").submit(function(e) {
		
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
        success: function (data) {
        if(data.status==200) {
           toastr.success(data.message);
           $(':input[type="submit"]').prop('disabled', false);
  				setTimeout(function(){
                 location.href="<?=base_url('User')?>";
           }, 1000) 
  
        }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
        }else{
           toastr.error('Something went wrong');
           $(':input[type="submit"]').prop('disabled', false);
        }
       },
       error: function(){} 
    });
   });
    </script>