<main class="main mainheight">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 position-relative ">
        <!-- Grid table-->
        <div class="card border-0 mb-4">
          <div class="card-header">
            <div class="row">
              <div class="col-auto mb-2">
                <i class="bi bi-shop h5 avatar avatar-40 bg-light-theme rounded"></i>
              </div>
              <div class="col align-self-center mb-2">
                <h6 class="d-inline-block mb-0"><?=$page_title?></h6>
              </div>
              <div class="col-12 col-sm-auto mb-2">
                <div class="rounded">
                  <a type="button" href="<?=base_url('add')?>" class="btn btn-primary">उपयोगकर्ता जोड़ें</a>
                </div>
              </div>
            </div>
          </div>
          <hr>
          <div class="col-md-6">
            <a href="javascript:void(0)" onclick="setRoleFilterSession(2)"
              class="btn btn-primary btn-sm <?=$this->session->userdata('role_filter')== 2 ? 'active' : 'badge badge-light-primary' ?>">All</a>

            <a href="javascript:void(0)" onclick="setRoleFilterSession(1)"
              class="btn btn-success btn-sm <?=$this->session->userdata('role_filter')== 1 ? 'active' : 'badge badge-light-success' ?>">सक्रिय</a>

            <a href="javascript:void(0)" onclick="setRoleFilterSession(0)"
              class="btn btn-danger btn-sm <?=$this->session->userdata('role_filter')== 0 ? 'active' : 'badge badge-light-danger' ?>">निष्क्रिय</a>
          </div>


          <div class="card-body p-3">

            <table class="table table-striped text-nowrap table-bordered vvv" style="width:100%" id="userDatatable">
              <thead>
                <tr class="text-muted">
                  <th class="w-12">क्रमांक</th>
                  <th class="">नाम</th>
                  <th>ईमेल</th>
                  <th data-breakpoints="xs sm">फ़ोन नंबर</th>
                  <th data-breakpoints="xs">दर्जा</th>
                  <th>कार्य</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($users as $key=>$user){?>
                <tr>
                  <td><?=$key+1?></td>
                  <td><?=$user->name.' '.$user->last_name?></td>
                  <td>
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <figure class="avatar avatar-50 mb-0 coverimg rounded-circle">
                          <img src="<?=$user->profile_pic ?>" alt="" />
                        </figure>
                      </div>
                      <div class="col ps-0">
                        <p class="mb-0"><?=$user->email?></p>
                        <b class="small"><?=$user->user_type?></b>
                      </div>
                    </div>
                  </td>
                  <td>
                    <p class="text-secondary small"><?=$user->contact?></p>
                  </td>
                  <td class="text-secondary" onclick="updateUserStatus(<?=$user->id ?>,<?=$user->status?>)"><input
                      type="checkbox" <?=$user->status == 1 ? 'checked' : ''?> data-toggle="toggle" data-on="सक्रिय"
                      data-off="निष्क्रिय" data-size="sm">
                  </td>
                  <td>
                    <a href="<?=base_url('edit/'.base64_encode($user->id))?>" title=" संपादित करें"><i
                        class="bi bi-pencil-square"></i></a>
                    <a class="text-danger" href="javascript:void(0)" onclick="deleteUser(<?=$user->id ?>)"
                      title="मिटायें"><i class="bi bi-trash"></i></a>
                  </td>
                </tr>
                <?php }?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
  new DataTable('#userDatatable');

  function updateUserStatus(id, status) {
    //alert(status); die;
    var messageText = "आप इस उपयोगकर्ता को सक्रिय करना चाहते हैं?";
    var confirmText = 'हाँ, इसे बदलो!';
    var message = "उपयोगकर्ता सक्रिय सफलतापूर्वक!";
    Swal.fire({
      title: 'क्या आपको यकीन है?',
      text: messageText,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: confirmText
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: '<?=base_url('User/update_status')?>',
          method: 'POST',
          data: {
            id,
            status
          },
          success: function(result) {
            toastr.success(message);
            setTimeout(function() {
              window.location.reload();
            }, 2000);
          }

        });

      } else {
        location.reload();
      }
    })
  }


  function deleteUser(id) {
    var messageText = "आप इस उपयोगकर्ता को हटाना चाहते हैं?";
    var confirmText = 'हाँ, इसे बदलो!';
    var message = "उपयोगकर्ता सफलतापूर्वक हटाएँ!";
    Swal.fire({
      title: 'क्या आपको यकीन है?',
      text: messageText,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: confirmText
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: '<?=base_url('User/delete')?>',
          method: 'POST',
          data: {
            id,
            status
          },
          success: function(result) {
            toastr.success(message);
            setTimeout(function() {
              window.location.reload();
            }, 2000);
          }

        });

      } else {
        location.reload();
      }
    })
  }

  function setRoleFilterSession(type){
    $.ajax({
      url: '<?=base_url('User/setRoleFilterSession')?>',
      type: 'POST',
      data: {
        type
      },
      success: function(data) {
       location.reload();  
      }
    });
  }
  </script>
  <script>
  new DataTable('#example');
  </script>