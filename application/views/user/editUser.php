<main class="main mainheight">
        <div class="container">
            <h5 class="title"><?=$page_title?></h5>
            <div class="row justify-content-center">
                <div class="col-12 col-lg-7 col-xl-8 mb-4">
                    <h6 class="title">मूल जानकारी</h6>
                    <form action="<?=base_url('User/update')?>" id="editForm" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="mb-2">
                            <div class="form-group mb-3 position-relative check-valid">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-text text-theme border-end-0"><i class="bi bi-person"></i></span>
                                    <div class="form-floating">
                                        <input type="text" placeholder="First Name" name="firstName" id="firstName" value="<?=$user->name?>" class="form-control border-start-0">
                                        <label>नाम</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-2">
                        <div class="form-group mb-3 position-relative check-valid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class="bi bi-envelope"></i></span>
                                <div class="form-floating">
                                    <input type="text" placeholder="Email address" name="email" id="email" value="<?=$user->email?>" class="form-control border-start-0">
                                    <label>मेल पता</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="mb-2">
                        <div class="form-group mb-3 position-relative check-valid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class="bi bi-phone"></i></span>
                                <div class="form-floating">
                                    <input type="text" placeholder="" name="contact" id="contact" value="<?=$user->contact?>" class="form-control border-start-0" maxlength="10" minlength="10"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                    <label>फ़ोन नंबर</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <div class="form-group mb-3 position-relative check-valid">
                            <div class="input-group input-group-lg">
                                <span class="input-group-text text-theme border-end-0"><i class="bi bi-person"></i></span>
                                <div class="form-floating">
                                  <select type="text" name="userType" id="userType" class="form-control border-start-0">
                                    <label>उपयोगकर्ता का प्रकार</label>
                                    <option value="">उपयोगकर्ता प्रकार चुनें</option>
                                    <option value="व्यवस्थापक" <?=$user->user_type == 'व्यवस्थापक' ? 'selected' : '' ?>>व्यवस्थापक</option>
                                    <option value="उपयोगकर्ता" <?=$user->user_type == 'उपयोगकर्ता' ? 'selected' : '' ?>>उपयोगकर्ता</option>
                                  </select>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <button class="btn btn-theme">अपडेट करना</button>
                </form>
                </div>
               
            
            </div>

        </div>

        <script>
    $("form#editForm").submit(function(e) {
		
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
    formData.append('id','<?=$user->id?>');
    formData.append('status','<?=$user->status?>');
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
        success: function (data) {
        if(data.status==200) {
           toastr.success(data.message);
           $(':input[type="submit"]').prop('disabled', false);
  				setTimeout(function(){
                 location.href="<?=base_url('User')?>";
           }, 1000) 
  
        }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
        }else{
           toastr.error('Something went wrong');
           $(':input[type="submit"]').prop('disabled', false);
        }
       },
       error: function(){} 
    });
   });
    </script>