<!doctype html>
<html lang="en" class="dark-mode">
<!-- Mirrored from maxartkiller.com/website/adminuxmobile2/html/home.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 30 Jun 2023 10:16:05 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
    <title><?=$page_title?></title>

    <!-- manifest meta -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="manifest" href="manifest.json" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="<?=base_url('public/assets/img/favicon180.png')?>" sizes="180x180">
    <link rel="icon" href="<?=base_url('public/assets/img/favicon32.png')?>" sizes="32x32" type="image/png">
    <link rel="icon" href="<?=base_url('public/assets/img/favicon16.png')?>" sizes="16x16" type="image/png">

    <!-- Google fonts-->
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
   
    <!-- bootstrap icons -->
     <link rel="stylesheet" href="<?=base_url('public/assets/bootstrap-icons.css') ?>">

    <!-- chosen css -->
    <link rel="stylesheet" href="<?=base_url('public/assets/vendor/chosen_v1.8.7/chosen.min.css')?>">

    <!-- date range picker -->
    <link rel="stylesheet" href="<?=base_url('public/assets/vendor/daterangepicker/daterangepicker.css')?>">

    <!-- swiper carousel css -->
    <link rel="stylesheet" href="<?=base_url('public/assets/vendor/swiper-7.3.1/swiper-bundle.min.css')?>">

    <!-- simple lightbox css -->
    <link rel="stylesheet" href="<?=base_url('public/assets/vendor/simplelightbox/simple-lightbox.min.css')?>">

    <!-- app tour css -->
    <link rel="stylesheet" href="<?=base_url('public/assets/vendor/Product-Tour-Plugin-jQuery/lib.css')?>">

     <!-- Footable table master css -->
     <link rel="stylesheet" href="<?=base_url('public/assets/vendor/fooTable/css/footable.bootstrap.min.css')?>">

    <!-- style css for this template -->
    <link href="<?=base_url('public/assets/scss/style.css')?>" rel="stylesheet">
    <link real="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" >
    <link real="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" >
    <link real="stylesheet" href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap4.min.css" >
    <link real="stylesheet" href="">
</head>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

  <script>
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="<?=base_url('public/admin/assets/plugins/datatables/datatables.min.css')?>">
	
  <!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script> -->
  <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.5/js/dataTables.bootstrap4.min.js"></script>


