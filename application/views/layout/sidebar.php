<!-- Sidebar -->
<div class="sidebar-wrap">
  <div class="sidebar">
    <div class="container">
      <div class="row mb-4">
        <div class="col align-self-center">
          <h6>मुख्य नेविगेशन</h6>
        </div>
        <div class="col-auto">
          <a class="" data-bs-toggle="collapse" data-bs-target="#usersidebarprofile" aria-expanded="false" role="button"
            aria-controls="usersidebarprofile">
            <i class="bi bi-person-circle"></i>
          </a>
        </div>
      </div>

      <!-- user information -->
      <div class="row text-center collapse " id="usersidebarprofile">
        <!--<div class="col-12">-->
        <!--    <div class="avatar avatar-100 rounded-circle shadow-sm mb-3 bg-white">-->
        <!--        <figure class="avatar avatar-90 rounded-circle coverimg">-->
        <!--            <img src="<?=base_url('public/assets/img/user-1.jpg')?>" alt="" id="userphotoonboarding">-->
        <!--        </figure>-->
        <!--    </div>-->
        <!--</div>-->
        <div class="col-12 mb-4">
          <h6 class="mb-1" id="usernamedisplay"><?=$this->session->userdata('email')?></h6>
          <p class="text-secondary small"><?=$this->session->userdata('name')?></p>
        </div>
      </div>

      <!-- user menu navigation -->
      <div class="row mb-4">
        <div class="col-12 px-0">
          <ul class="nav nav-pills">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="<?=base_url('home')?>">
                <div class="avatar avatar-40 icon"><i class="bi bi-house-door"></i></div>
                <div class="col">होम</div>
                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
              </a>
            </li>

            <?php if($this->session->userdata('user_type')=='व्यवस्थापक'){?>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" data-bs-auto-close="false"
                data-bs-display="static" href="javascript:void(0)" role="button" aria-expanded="false">
                <div class="avatar avatar-40 icon"><i class="bi bi-person-square"></i></div>
                <div class="col">उपयोगकर्ता</div>
                <div class="arrow"><i class="bi bi-chevron-down plus"></i> <i class="bi bi-chevron-up minus"></i>
                </div>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a class="dropdown-item nav-link" href="<?=base_url('users')?>">
                    <div class="avatar avatar-40 icon"><i class="bi bi-grid"></i>
                    </div>
                    <div class="col align-self-center">उपयोगकर्ता सूची</div>
                    <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                  </a>
                </li>
                <li>
                  <a class="dropdown-item nav-link" href="<?=base_url('add')?>">
                    <div class="avatar avatar-40 icon"><i class="bi bi-person-square"></i>
                    </div>
                    <div class="col align-self-center">उपयोगकर्ता को जोड़ें</div>
                    <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                  </a>
                </li>

              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" data-bs-auto-close="false"
                data-bs-display="static" href="#javascript:void(0)" role="button" aria-expanded="false">
                <div class="avatar avatar-40 icon"><i class="bi bi-boxes"></i></div>
                <div class="col">क्षेत्र</div>
                <div class="arrow"><i class="bi bi-chevron-down plus"></i> <i class="bi bi-chevron-up minus"></i>
                </div>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a class="dropdown-item nav-link" href="<?=base_url('block')?>">
                    <div class="avatar avatar-40 icon"><i class="bi bi-box"></i>
                    </div>
                    <div class="col align-self-center">विकास खण्ड</div>
                    <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                  </a>
                </li>
                <li>
                  <a class="dropdown-item nav-link" href="<?=base_url('village')?>">
                    <div class="avatar avatar-40 icon"><i class="bi bi-clipboard-check"></i>
                    </div>
                    <div class="col align-self-center">ग्राम</div>
                    <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="<?=base_url('sections')?>">
                <div class="avatar avatar-40 icon"><i class="bi bi-layers"></i></div>
                <div class="col">सेक्शन</div>
                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="<?=base_url('acts')?>">
                <div class="avatar avatar-40 icon"><i class="bi bi-card-list"></i></div>
                <div class="col">एक्ट्स</div>
                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
              </a>
            </li>
            <?php } ?>
            <!--<li class="nav-item dropdown">-->
            <!--    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" data-bs-auto-close="false" data-bs-display="static" href="#javascript:void(0)" role="button" aria-expanded="false">-->
            <!--        <div class="avatar avatar-40 icon"><i class="bi bi-boxes"></i></div>-->
            <!--        <div class="col">वाद</div>-->
            <!--        <div class="arrow"><i class="bi bi-chevron-down plus"></i> <i class="bi bi-chevron-up minus"></i>-->
            <!--        </div>-->
            <!--    </a>-->
            <!--<ul class="dropdown-menu">-->
            <!--    <li>-->
            <!--        <a class="dropdown-item nav-link" href="<?=base_url('litigant')?>">-->
            <!--            <div class="avatar avatar-40 icon"><i class="bi bi-grid"></i>-->
            <!--            </div>-->
            <!--            <div class="col align-self-center">वाद सूची</div>-->
            <!--            <div class="arrow"><i class="bi bi-chevron-right"></i></div>-->
            <!--        </a>-->
            <!--    </li>-->
            <!-- <li>
                                        <a class="dropdown-item nav-link" href="<?=base_url('litigant')?>">
                                            <div class="avatar avatar-40 icon"><i class="bi bi-cloud-upload"></i>
                                            </div>
                                            <div class="col align-self-center">Bulk Upload</div>
                                            <div class="arrow"><i class="bi bi-chevron-right"></i></div>
                                        </a>
                                    </li> -->
            <!--</ul>-->
            <!--</li>  -->
          </ul>
        </div>
      </div>


      <!-- Quick links -->
      <div class="row mb-3">
        <div class="col align-self-center">
          <h6>Quick Links</h6>
        </div>
        <div class="col-auto">
        </div>
      </div>
      <div class="row mb-4">
        <div class="col-12 px-0">
          <ul class="nav nav-pills">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="<?=base_url('authantication/logout')?>">
                <div class="avatar avatar-40 icon"><i class="bi bi-journal-code"></i></div>
                <div class="col">लॉगआउट</div>
                <div class="arrow"><i class="bi bi-chevron-right"></i></div>
              </a>
            </li>
          </ul>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- Sidebar ends -->