<!-- Required jquery and libraries -->
<script src="<?=base_url('public/assets/js/jquery-3.3.1.min.js')?>"></script>
    <script src="<?=base_url('public/assets/js/popper.min.js')?>"></script>
    <script src="<?=base_url('public/assets/vendor/bootstrap-5/dist/js/bootstrap.bundle.js')?>"></script>

    <!-- Customized jquery file  -->
    <script src="<?=base_url('public/assets/js/main.js')?>"></script>
    <script src="<?=base_url('public/assets/js/color-scheme.js')?>"></script>

    <!-- PWA app service registration and works -->
    <script src="<?=base_url('public/assets/js/pwa-services.js')?>"></script>

    <!-- Chart js script -->
    <script src="<?=base_url('public/assets/vendor/chart-js-3.3.1/chart.min.js')?>"></script>

    <!-- Progress circle js script -->
    <script src="<?=base_url('public/assets/vendor/progressbar-js/progressbar.min.js')?>"></script>

    <!-- swiper js script -->
    <script src="<?=base_url('public/assets/vendor/swiper-7.3.1/swiper-bundle.min.js')?>"></script>

    <!-- page level script -->
    <script src="<?=base_url('public/assets/js/login.js')?>"></script>

</body>


<!-- Mirrored from maxartkiller.com/website/adminuxmobile2/html/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 30 Jun 2023 10:16:59 GMT -->
</html>