
<body class="d-flex flex-column h-100 sidebar-pushcontent sidebar-filled" data-sidebarstyle="sidebar-pushcontent">

<!-- page loader -->
<div class="container-fluid h-100 position-fixed loader-wrap bg-blur">
    <div class="row justify-content-center h-100">
        <div class="col-auto align-self-center text-center px-5 leaf">
            <h2 class="mb-1"><?=$page_title?><b class="fw-bold"></b></h2>
            <div class="logo-square animated mb-4">
                <div class="icon-logo">
                    <img src="<?=base_url('public/assets/img/logo-icon.png')?>" alt="" />
                </div>
            </div>
            <br>
        </div>
    </div>
</div>
<body class="hold-transition light-skin sidebar-mini theme-primary fixed">
   <div class="wrapper">
	 <div id="loader"></div>
<!-- page loader ends -->
<!-- Header -->
<header class="header">
        <!-- Fixed navbar -->
        <nav class="navbar fixed-top">
            <div class="container-fluid">
                <button class="btn btn-link btn-square menu-btn me-2" type="button">
                    <i class="bi bi-list fs-4"></i>
                </button>
                <div class="header-title align-self-center">
                    <h5 class="mb-0"><?=$page_title?></h5>
                </div>
                <div class="ms-auto">
                    <div class="row">
                        <div class="col-auto align-self-center">
                            <div class="dropdown">
                                <a class="dd-arrow-none dropdown-toggle" id="userprofiledd" data-bs-toggle="dropdown" aria-expanded="false" role="button">
                                    <div class="row">
                                        <div class="col-auto align-self-center">
                                            <figure class="avatar avatar-30 rounded-circle coverimg vm">
                                                <img src="<?=base_url('public/assets/img/user-1.jpg')?>" alt="" id="userphotoonboarding2" />
                                            </figure>
                                        </div>
                                        <div class="col ps-0 align-self-center d-none d-lg-block">
                                            <p class="mb-0">
                                                <span class="text-dark username"><?=$this->session->userdata('name')?></span><br>
                                                <!-- <small class="small"><?=$this->session->userdata('email')?></small> -->
                                            </p>
                                        </div>
                                        <div class="col ps-0 align-self-center d-none d-lg-block">
                                            <i class="bi bi-chevron-down small vm"></i>
                                        </div>
                                    </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-end w-300" aria-labelledby="userprofiledd">
                                    <div class="dropdown-info bg-radial-gradient-theme">
                                        <div class="row">
                                            <div class="col-auto">
                                                <figure class="avatar avatar-50 rounded-circle coverimg vm">
                                                    <img src="<?=base_url('public/assets/img/user-1.jpg')?>" alt="" id="userphotoonboarding3" />
                                                </figure>
                                            </div>
                                            <div class="col align-self-center ps-0">
                                                <h6 class="mb-0"><span class="username"><?=$this->session->userdata('name')?></span></h6>
                                                <p class="text-muted small"><?=$this->session->userdata('email')?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div><a class="dropdown-item" href="<?=base_url('authantication/logout')?>">लॉग आउट</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <!-- Header ends -->