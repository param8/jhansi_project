<!DOCTYPE html>
<html lang="en">
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=5, user-scalable=1" name="viewport" />
    <meta name="csrf-token" content="AOon20Bb4mYyagwLmaMmirPQ5QGYejseiIYqDpoJ">
    <link href="https://fonts.googleapis.com/css?family=Poppins:ital,wght@0,400;0,500;0,600;0,700;1,400&amp;display=swap" rel="stylesheet">
    <!-- crasoul -->
    <!-- bbbb -->
    <style>
      table tr td{
      border: 1px solid #26237c;    padding: 5px;
      }
      .b_none tr td {
      border:none;
      }
      .b-tr td{
      border:none
      }
      .b-tr{
      border-top:1px solid #26237c!important;
      }
    </style>
  </head>
  <body class="  " style="font-family: math;border:3px solid #26237c; width:715px">
    <table class="table-responsive" style="width:715px">
      <tr style="border: none; ">
        <td style="padding: 0;border: none;width:700px" colspan="3">
          <img src="<?=base_url('public/header.PNG')?>" alt="logo" style="width:100%">
          <hr>
        </td>
      </tr>
      <tr  class="b-tr" colspan="3" style="border-bottom:1px solid #26237c!important;">
        <td colspan="3" style="text-align: center;"><span style="width: 300px; font-weight: bold; border:none;">TAX INVOICE  </span></td>
      </tr>
      <tr class="b-tr">
        <td colspan="2">
          <div  style="width: 300px;">
            <p style="margin: 0;"><b>AEGLE TOURS PRIVATE LIMITED</b>
              <br>
              AMBER VIHAR-7, H NO 26, AMANPUR, Jabalpur, Jabalpur,
              Madhya Pradesh,<br> 482001 <br>
             
              State: Madhya Pradesh
            </p>
            <p style="margin: 0;">
              <b>
                GSTIN: 23AAXCA9461H1Z6<br>
                <!-- CI Number: U74999DL2006PTC155233<br>
                  PAN : AACCT6259K -->
              </b>
            </p>
          </div>
        </td>
        <td>
          <div style="width: 300px; text-align: left; margin-left:12px;">
            <b>Invoice No</b> : <?=$invoice->invoiceNo?><br>
            <b>Invoice Date</b> : <?=date('d-M-Y',strtotime($invoice->created_at))?><br>
            <b>PNR</b> :   <?=$invoice->pnr?>
          </div>
          <div style="width: 300px; text-align: right;">
            <p><?=$invoice->userAddress?></p>
            <p>
              <b>Phone</b> : <?=$invoice->userContact?><br>
              <b>State</b> : <?=$invoice->stateName?><br>
              <b>PAN</b> :   <?=$invoice->userPancard?><br>
              <b>GSTIN</b> :   <?=$invoice->UsergstNo?><br>
              <?php 
                $userGST = !empty($invoice->UsergstNo) ? substr($invoice->UsergstNo,0,2) : '';?>
            </p>
          </div>
        </td>
      </tr>
    </table>
    <table  style=" border-spacing: 0;">
      <tr style="width:700px">
        <td><b class="t_h" style="font-size: 10px;">S  No. </b></td>
        <td><b class="t_h" style="font-size: 10px;">Ticket  No  </b></td>
        <td><b class="t_h" style="font-size: 10px;">Sectors </b></td>
        <td><b class="t_h" style="font-size: 10px;">Air  Line</b></td>
        <td> <b class="t_h" style="font-size: 10px;">PAX Name </b></td>
        <td> <b class="t_h" style="font-size: 10px;">Flight No. </b></td>
        <td> <b class="t_h" style="font-size: 10px;">Class </b></td>
        <td> <b class="t_h" style="font-size: 10px;">Fare </b></td>
        <td> <b class="t_h" style="font-size: 10px;">Tax </b></td>
        <td> <b class="t_h" style="font-size: 10px;">Bag.Ch. </b></td>
        <td> <b class="t_h" style="font-size: 10px;">Meal   Ch. </b></td>
        <td> <b class="t_h" style="font-size: 10px;">Seat  Ch.     </b> </td>
        <td> <b class="t_h" style="font-size: 10px;">Sp   Service  Ch. </b></td>
        <td> <b class="t_h" style="font-size: 10px;">Global   Pr.   Ch. </b></td>
      </tr>
      <tr>
        <td><b class="t_h" style="font-size: 10px;">1 </b></td>
        <td><b class="t_h" style="font-size: 10px;"><?=$invoice->ticketNo?>  </b></td>
        <td><b class="t_h" style="font-size: 10px;"><?=$invoice->sectors?> </b></td>
        <td><b class="t_h" style="font-size: 10px;"><?=$invoice->flight?></b></td>
        <td> <b class="t_h" style="font-size: 10px;"><?=$invoice->customerName?> </b>
        </td>
        <td> <b class="t_h" style="font-size: 10px;"><?=$invoice->type?>     </b></td>
        <td> <b class="t_h" style="font-size: 10px;"><?=$invoice->class?> </b></td>
        <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->fare,2)?> </b></td>
        <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->tax,2)?> </b></td>
        <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->bag_cahrge,2)?> </b></td>
        <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->meal_charge,2)?> </b></td>
        <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->seat_charge,2)?>     </b> </td>
        <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->sp_service_charge,2)?> </b></td>
        <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->globle_pr_charge,2)?>  </b></td>
      </tr>
    </table>
    <table>
      <tr >
        <td  style="width:700px">
          <table class="b_none" style="width:715px">
            <tbody>
              <tr>
                <td rowspan="6" style="width:350px">
                  <h3>Account Detail</h3>
                  <span>Account No. </span> : 019805009284<br>
                  <span>IFS Code </span> : ICIC0000198<br>
                  <span>Name  </span> : Aegle Tours Private Limited <br>
                  <span>Branch </span> Jabalpur
                </td>
                <?php 
                  $grossTotal = array_sum(array($invoice->fare,$invoice->tax,$invoice->bag_cahrge,$invoice->meal_charge,$invoice->seat_charge,$invoice->sp_service_charge,$invoice->globle_pr_charge));
                  $totalgst = !empty($invoice->service_charge) ?  round(($invoice->service_charge/118)*100,2) : 0;
                  $igst = !empty($invoice->service_charge) ? $invoice->service_charge-$totalgst : 0;
                  $cgst_sgst = !empty($invoice->service_charge) ? $igst/2 : 0;
                  ?>
                <td colspan="2" class="text-center"><b>Gross: </b></td>
                <td><b><?= number_format($grossTotal,2);?></b></td>
              </tr>
              <tr>
                <td rowspan="" style="    vertical-align: sub;">Add : </td>
                <td>Service Charges </td>
                <td> <?=number_format($totalgst,2)?></td>
              </tr>
              <?php if($userGST == 23){?>
              <tr>
                <td rowspan="" style="    vertical-align: sub;">Add : </td>
                <td>CGST @9%  </td>
                <td> <?=number_format($cgst_sgst,2)?></td>
              </tr>
              <tr>
                <td rowspan="" style="    vertical-align: sub;">Add : </td>
                <td>SGST @9%  </td>
                <td> <?=number_format($cgst_sgst,2)?></td>
              </tr>
              <?php } else{?>
              <tr>
                <td rowspan="" style="    vertical-align: sub;">Add : </td>
                <td>IGST @18%  </td>
                <td> <?=number_format($igst,2)?></td>
              </tr>
              <?php } ?>
              <tr>
                <td rowspan="" style="    vertical-align: sub;">Add : </td>
                <td><b>Net Amount </b>  </td>
                <td><b><?=number_format($grossTotal+$invoice->service_charge,2)?></b></td>
              </tr>
              <tr>
                <td colspan="3"><span class="" style="border: 1px dotted #555;width: 92%;
                  display: block;"></span></td>
              </tr>
              <tr>
                <td colspan="">Note :  </td>
                <td colspan="2"> (Amount in Rs.)
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>

      <tr>
        <td colspan="2" style="border: none;">
          <div class="by_tract">
            <p style="display: flex;gap: 15px;"><span>IMP </span> : <span><small>This is computer generated invoice signature not required</small></span></p>
            <p style="display: flex;gap: 15px;"><span>IMP </span> : <span><small>All Cases & Disputes are subject to New Delhi Jurisdiction</small></span></p>
            <p style="display: flex;gap: 15px;"><span>IMP  </span> : <span><small>Refunds & Cancellations are subject to Airlines approval .</small></span></p>
            <p style="display: flex;gap: 15px;"><span>IMP  </span> : <span><small>Service charges as included above are to be collected from the customers on our behalf.                    </small></span></p>
            <p style="display: flex;gap: 15px;"><span>CHEQUE  </span> : <span><small>Must be drawn in favour of 'AEGLE TOURS PRIVATE LIMITED'.
              </small></span>
            </p>
            <!-- <p style="display: flex;gap: 15px;"><span>LATE PAYMENT  </span> : <span><small> Interest @ 24% per annum will be charged on all outstanding bills after due date.
              </small></span>
              </p> -->
            <p style="display: flex;gap: 15px;"><span>VERY IMP  </span> : <span><small>Kindly check all details carefully to avoid un‑necessary complications.</small></span></p>
          </div>
        </td>
      </tr>
    </table>
  </body>
</html>