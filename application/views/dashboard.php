 <!-- Begin page content -->
 <main class="main mainheight">
        <!-- page title bar -->
        <!-- <div class="container-fluid mb-4">
            <div class="row align-items-center page-title">
                <div class="col col-sm-auto">
                    <div class="input-group input-group-md">
                        <input type="text" class="form-control bg-none px-0" value="" id="titlecalendar" />
                        <span class="input-group-text text-secondary bg-none" id="titlecalandershow"><i class="bi bi-calendar-event"></i></span>
                    </div>
                </div>
            </div>
        </div> -->

        <!-- welcome bar -->
        <div class="container">
            <div class="row">
                <div class="col-12 rounded">
                    <div class="row align-items-center">

                        <!-- welcome message -->
                        <!-- <div class="col-12 col-md-7 py-3 py-md-5">
                            <h2 class="fw-light mb-0 text-secondary">Welcome,</h2>
                            <h1 class="display-3 username mb-3"><?=$this->session->userdata('name')?></h1>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- content -->
         <div class="container">
         <div class="row">
            <div class="row mb-4 py-2">
                <div class="col-12 text-center">
                    <h2>स्वागत</h2>
                    <h1 class="display-3 username mb-3"><?=$this->session->userdata('name')?></h1>  
                </div>
            </div>
         </div>
           <!-- <div class="row">-->
                <!-- summary blocks -->
                <!--<div class="col-6 col-md-6 col-lg-6 col-xxl-3">
                    <div class="card border-0 mb-4">
                        <div class="card-body">
                            <div class="row align-items-center">-->
                                <!-- <div class="col-12 text-center mb-3">
                                    <div class="circle-small mx-auto">
                                        <div id="circleprogressblue"></div>
                                        <div class="avatar h5 bg-light-blue rounded-circle">
                                            <i class="bi bi-calendar2-check"></i>
                                        </div>
                                    </div>
                                </div> -->
                                <!--<div class="col">
                                    <p class="text-secondary small mb-1">Task Completed</p>
                                    <h5 class="fw-medium">60<small>%</small></h5>
                                </div>
                                <div class="col-auto">
                                    <div class="dropdown d-inline-block">
                                        <a class="text-secondary dd-arrow-none" data-bs-toggle="dropdown" aria-expanded="false" data-bs-display="static" role="button">
                                            <i class="bi bi-three-dots-vertical"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-end">
                                            <li><a class="dropdown-item" href="javascript:void(0)">Edit</a></li>
                                            <li><a class="dropdown-item" href="javascript:void(0)">Move</a></li>
                                            <li><a class="dropdown-item text-danger" href="javascript:void(0)">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-6 col-lg-6 col-xxl-3">
                    <div class="card border-0 mb-4">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-12 text-center mb-3">
                                    <div class="circle-small mx-auto">
                                        <div id="circleprogressyellow"></div>
                                        <div class="avatar h5 bg-light-yellow text-yellow rounded-circle">
                                            <i class="bi bi-building"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <p class="text-secondary small mb-1">Construction</p>
                                    <h5 class="fw-medium">12550<small>USD</small></h5>
                                </div>
                                <div class="col-auto">
                                    <div class="dropdown d-inline-block">
                                        <a class="text-secondary dd-arrow-none" data-bs-toggle="dropdown" aria-expanded="false" data-bs-display="static" role="button">
                                            <i class="bi bi-three-dots-vertical"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-end">
                                            <li><a class="dropdown-item" href="javascript:void(0)">Edit</a></li>
                                            <li><a class="dropdown-item" href="javascript:void(0)">Move</a></li>
                                            <li><a class="dropdown-item text-danger" href="javascript:void(0)">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-6 col-lg-6 col-xxl-3">
                    <div class="card border-0 mb-4">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-12 text-center mb-3">
                                    <div class="avatar avatar-50 h5 bg-light-red text-red rounded-circle mx-auto">
                                        <i class="bi bi-emoji-heart-eyes"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <p class="text-secondary small mb-1">Event Joined</p>
                                    <h5 class="fw-medium">1525<small>k</small></h5>
                                </div>
                                <div class="col-auto">
                                    <div class="dropdown d-inline-block">
                                        <a class="text-secondary dd-arrow-none" data-bs-toggle="dropdown" aria-expanded="false" data-bs-display="static" role="button">
                                            <i class="bi bi-three-dots-vertical"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-end">
                                            <li><a class="dropdown-item" href="javascript:void(0)">Edit</a></li>
                                            <li><a class="dropdown-item" href="javascript:void(0)">Move</a></li>
                                            <li><a class="dropdown-item text-danger" href="javascript:void(0)">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-6 col-lg-6 col-xxl-3">
                    <div class="card border-0 mb-4">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-12 text-center mb-3">
                                    <div class="avatar avatar-50 h5 bg-green text-white rounded-circle mx-auto">
                                        <i class="bi bi-thermometer-sun"></i>
                                    </div>
                                </div>
                                <div class="col">
                                    <p class="text-secondary small mb-1">Temperature</p>
                                    <h5 class="fw-medium">45 <small><sup>0</sup>C, R-32</small></h5>
                                </div>
                                <div class="col-auto">
                                    <div class="dropdown d-inline-block">
                                        <a class="text-secondary dd-arrow-none" data-bs-toggle="dropdown" aria-expanded="false" data-bs-display="static" role="button">
                                            <i class="bi bi-three-dots-vertical"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-end">
                                            <li><a class="dropdown-item" href="javascript:void(0)">Edit</a></li>
                                            <li><a class="dropdown-item" href="javascript:void(0)">Move</a></li>
                                            <li><a class="dropdown-item text-danger" href="javascript:void(0)">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6 col-xxl-3 mb-4 ">-->
                    <!-- finance card -->
                    <!--<div class="card border-0 bg-gradient-theme-light theme-blue">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h6 class="fw-medium">
                                        <i class="bi bi-cash h5 me-1 avatar avatar-40 bg-light-theme rounded me-2"></i>
                                        Finance
                                    </h6>
                                </div>
                                <div class="col-auto">
                                </div>
                            </div>
                        </div>
                        <div class="card-body px-0">
                            <div class="swiper-container creditcards">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="card border-0 mb-2 theme-blue bg-radial-gradient">
                                            <div class="card-body">
                                                <div class="row align-items-center mb-4">
                                                    <div class="col-auto align-self-center">
                                                        <img src="assets/img/visa.png" alt="">
                                                    </div>
                                                    <div class="col text-end">
                                                        <p class="size-12">
                                                            <span class="text-muted small">City Bank</span><br>
                                                            <span class="">Credit Card</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="fw-medium h6 mb-3">
                                                    000 0000 0001 546598
                                                </p>
                                                <div class="row">
                                                    <div class="col-auto size-12">
                                                        <p class="mb-0 text-muted small">Expiry</p>
                                                        <p>09/023</p>
                                                    </div>
                                                    <div class="col text-end size-12">
                                                        <p class="mb-0 text-muted small">Card Holder</p>
                                                        <p>Maxartkiller</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row amount-data">
                                            <div class="col">
                                                <p class="text-secondary small mb-1">Expense</p>
                                                <p>1500.00 <small class="text-success">18.0% <i class="bi bi-arrow-up"></i></small></p>
                                            </div>
                                            <div class="col-auto text-end">
                                                <p class="text-secondary small mb-1">Limit Remain</p>
                                                <p>13500.00</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="card border-0 theme-pink bg-radial-gradient mb-2">
                                            <div class="card-body">
                                                <div class="row align-items-center mb-4">
                                                    <div class="col-auto align-self-center">
                                                        <img src="assets/img/visa.png" alt="">
                                                    </div>
                                                    <div class="col text-end">
                                                        <p class="size-12">
                                                            <span class="text-muted small">City Bank</span><br>
                                                            <span class="">Credit Card</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="fw-medium h6 mb-3">
                                                    000 0000 0001 546598
                                                </p>
                                                <div class="row">
                                                    <div class="col-auto size-12">
                                                        <p class="mb-0 text-muted small">Expiry</p>
                                                        <p>09/023</p>
                                                    </div>
                                                    <div class="col text-end size-12">
                                                        <p class="mb-0 text-muted small">Card Holder</p>
                                                        <p>Maxartkiller</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row amount-data">
                                            <div class="col">
                                                <p class="text-secondary small mb-1">Expense</p>
                                                <p>3650.00 <small class="text-danger">11.0% <i class="bi bi-arrow-down"></i></small></p>
                                            </div>
                                            <div class="col-auto text-end">
                                                <p class="text-secondary small mb-1">Limit Remain</p>
                                                <p>35500.00</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="card border-0 theme-yellow bg-radial-gradient mb-2">
                                            <div class="card-body">
                                                <div class="row align-items-center mb-4">
                                                    <div class="col-auto align-self-center">
                                                        <img src="assets/img/visa.png" alt="">
                                                    </div>
                                                    <div class="col text-end">
                                                        <p class="size-12">
                                                            <span class="text-muted small">City Bank</span><br>
                                                            <span class="">Credit Card</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="fw-medium h6 mb-3">
                                                    000 0000 0001 546598
                                                </p>
                                                <div class="row">
                                                    <div class="col-auto size-12">
                                                        <p class="mb-0 text-muted small">Expiry</p>
                                                        <p>09/023</p>
                                                    </div>
                                                    <div class="col text-end size-12">
                                                        <p class="mb-0 text-muted small">Card Holder</p>
                                                        <p>Maxartkiller</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row amount-data">
                                            <div class="col">
                                                <p class="text-secondary small mb-1">Expense</p>
                                                <p>1500.00 <small class="text-success">18.0 <i class="bi bi-arrow-up"></i></small></p>
                                            </div>
                                            <div class="col-auto text-end">
                                                <p class="text-secondary small mb-1">Limit Remain</p>
                                                <p>13500.00</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card border-0 mb-2">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-auto">
                                            <div class="avatar avatar-50 h5 bg-light-theme text-theme rounded-circle">
                                                <i class="bi bi-receipt"></i>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <p class="text-secondary small mb-1">Billed Amount</p>
                                            <p>1525 <small>USD</small></p>
                                        </div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer justify-content-center text-center">
                            <a href="finance-dashboard.html" class="btn btn-sm btn-link text-theme">Visit Finance Dashboard <i class="bi bi-arrow-right vm"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6 col-xxl-3 mb-4 ">-->
                    <!-- Inventory card -->
                    <!--<div class="card border-0 bg-gradient-theme-light theme-yellow h-100">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h6 class="fw-medium">
                                        <i class="bi bi-box h5 me-1 avatar avatar-40 bg-light-theme rounded me-2"></i>
                                        Inventory
                                    </h6>
                                </div>
                                <div class="col-auto">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col-auto">
                                    <div class="rounded bg-theme text-white p-3">
                                        <p class="text-muted small mb-1">
                                            Annual<br />Income
                                        </p>
                                        <p>$124k</p>
                                    </div>
                                </div>
                                <div class="col align-self-center">
                                    <p class="text-secondary small mb-0">United States</p>
                                    <p>45<small>% Sales</small></p>

                                    <div class="mt-3">
                                        <div class="progress h-5 mb-1 bg-light-theme">
                                            <div class="progress-bar bg-theme" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                    <p class="small text-secondary">Targeted orders <span class="float-end">153k</span></p>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-auto">
                                    <div class="rounded bg-light-theme text-dark p-3">
                                        <p class="text-muted small mb-1">
                                            Annual<br />Income
                                        </p>
                                        <p>$124k</p>
                                    </div>
                                </div>
                                <div class="col align-self-center">
                                    <p class="text-secondary small mb-0">United Kingdom</p>
                                    <p>15<small>% Sales</small></p>

                                    <div class="mt-3">
                                        <div class="progress h-5 mb-1 bg-light-yellow">
                                            <div class="progress-bar bg-yellow" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                    <p class="small text-secondary">Targeted orders <span class="float-end">53k</span></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-md text-center">
                                    <i class="bi bi-box h5 avatar avatar-30 text-green mb-2"></i>
                                    <h4 class="increamentcount mb-0">1265</h4>
                                    <p class="small text-secondary">In Stock</p>
                                </div>
                                <div class="col col-md text-center">
                                    <i class="bi bi-truck h5 avatar avatar-30 text-theme mb-2"></i>
                                    <h4 class="increamentcount mb-0">365</h4>
                                    <p class="small text-secondary">Delivered</p>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer justify-content-center text-center">
                            <a href="inventory-dashboard.html" class="btn btn-sm btn-link text-yellow">Visit Inventory Dash <i class="bi bi-arrow-right vm"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6 col-xxl-3 mb-4 ">-->
                    <!-- Network card -->
                   <!-- <div class="card border-0 bg-gradient-theme-light theme-red h-100">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h6 class="fw-medium">
                                        <i class="bi bi-hdd-rack h5 me-1 avatar avatar-40 bg-light-theme rounded me-2"></i>
                                        Network
                                    </h6>
                                </div>
                                <div class="col-auto">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="smallchart mb-2">
                                <canvas id="smallchart2"></canvas>
                            </div>
                            <p class="mb-1">Server CPU <span class="text-secondary">#0514-R3D</span></p>
                            <p class="text-secondary">45% 3.2 MHz</p>
                            <div class="card border-0 mt-3">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-auto">
                                            <div class="circle-small">
                                                <div id="circleprogressred"></div>
                                                <div class="avatar h5 bg-light-theme text-theme rounded-circle">
                                                    <i class="bi bi-bug"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <p class="text-secondary small mb-1">Ticket Created</p>
                                            <p>651<small> and 250 yesterday</small></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col">
                                            <p class="text-secondary small mb-1">Resolved</p>
                                            <p class="text-success">432</p>
                                        </div>
                                        <div class="col border-left-dashed">
                                            <p class="text-secondary small mb-1">In Progress</p>
                                            <p class="text-warning">50</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer justify-content-center text-center">
                            <a href="network-dashboard.html" class="btn btn-sm btn-link text-theme">Visit Network Dashboard <i class="bi bi-arrow-right vm"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6 col-xxl-3 mb-4 ">-->
                    <!-- Social card -->
                   <!-- <div class="card border-0 bg-gradient-theme-light theme-green h-100">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h6 class="fw-medium">
                                        <i class="bi bi-people h5 me-1 avatar avatar-40 bg-light-theme rounded me-2"></i>
                                        Social
                                    </h6>
                                </div>
                                <div class="col-auto">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row align-items-center mb-3">
                                <div class="col">
                                    <figure class="coverimg rounded w-100 h-80 mb-0">
                                        <img src="assets/img/news-4.jpg" alt="" class="mw-100" />
                                    </figure>
                                </div>
                                <div class="col">
                                    <p class="text-secondary small mb-0">Boosted Post</p>
                                    <h4>2,545,05</h4>
                                    <p class="text-secondary small">People Reached</p>
                                </div>
                            </div> -->
                            <!-- <div class="row align-items-center">
                               <div class="col-12 mb-2">
                                    <p class="text-secondary small">Buy now or Share now! Do support </p>
                                </div>
                                <div class="col">
                                    <p class="small text-secondary mb-0">Tweet Likes</p>
                                    <p class="mb-0">65.15 k</p>
                                </div>
                                <div class="col">
                                    <p class="small text-secondary mb-0">Retweet</p>
                                    <p class="mb-0">8.2 k</p>
                                </div>
                                <div class="col">
                                    <p class="small text-secondary mb-0">Clicks</p>
                                    <p class="mb-0">52.01 k</p>
                                </div>
                            </div> -->

                            <!--<div class="card border-0 mt-3 bg-radial-gradient text-white">
                                <div class="card-body bg-none">
                                    <div class="row align-items-center mb-3">
                                        <div class="col-auto">
                                            <figure class="avatar avatar-50 coverimg rounded">
                                                <img src="assets/img/business-2.jpg" alt="" />
                                            </figure>
                                        </div>
                                        <div class="col">
                                            <p>Learn about software and Framework. All...</p>
                                        </div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="col-auto">
                                            <p class="mb-0">125</p>
                                            <p class="small">Reached</p>
                                        </div>
                                        <div class="col-auto ps-0">
                                            <p class="mb-0">35</p>
                                            <p class="small">Likes</p>
                                        </div>
                                        <div class="col text-end">
                                            <button class="btn btn-sm btn-light">Boost</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer justify-content-center text-center">
                            <a href="social-dashboard.html" class="btn btn-sm btn-link text-theme">Visit Social Dashboard <i class="bi bi-arrow-right vm"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-4 py-2">
                <div class="col text-center">
                    <h4>Important <span class="text-gradient">News</span> around you</h4>
                    <p class="text-secondary">While you are working hard, we let you know important news.</p>
                </div>
            </div>
            <div class="row">-->
                <!-- news and offers -->
                <!--<div class="col-12 col-lg-12 col-xl-4 col-xxl-4 mb-4 ">
                    <div class="card border-0">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-auto">
                                    <i class="bi bi-newspaper h5 me-1 avatar avatar-40 bg-light-theme rounded me-2"></i>
                                </div>
                                <div class="col ps-0">
                                    <h6 class="fw-medium mb-0">Great news</h6>
                                    <p class="text-secondary small">Change world is a combine effort.</p>
                                </div>
                                <div class="col-auto">
                                </div>
                            </div>
                        </div>
                        <div class="coverimg w-100 h-180 position-relative">
                            <div class="position-absolute bottom-0 start-0 w-100 mb-3 px-3 z-index-1">
                                <div class="row">
                                    <div class="col">
                                        <button class="btn btn-sm btn-outline-light btn-rounded">Share this</button>
                                    </div>
                                    <div class="col-auto">
                                        <div class="dropup d-inline-block">
                                            <a class="btn btn-square btn-sm rounded-circle btn-outline-light dd-arrow-none" data-bs-toggle="dropdown" aria-expanded="false" data-bs-display="static" role="button">
                                                <i class="bi bi-three-dots-vertical"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-end">
                                                <li><a class="dropdown-item" href="javascript:void(0)"><i class="bi bi-hand-thumbs-up me-1 text-green"></i> Recommendation this</a></li>
                                                <li><a class="dropdown-item" href="javascript:void(0)"><i class="bi bi-hand-thumbs-down me-1 text-danger"></i> Don't recommend</a></li>
                                                <li><a class="dropdown-item" href="javascript:void(0)"><i class="bi bi-star text-yellow"></i> Add to favorite</a></li>
                                                <li><a class="dropdown-item text-danger" href="javascript:void(0)">Report this</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <img src="assets/img/bg-20.jpg" class="mw-100" alt="" />
                        </div>
                        <div class="card-body">
                            <h5 class="mb-3">We all are artist in our field. We all are able to find symmetry in our routine</h5>
                            <h6 class="fw-medium mb-2">Make it clutter free and create better world</h6>
                            <p class="text-secondary">We have added useful and wider-range of widgets fully flexible with wrapper container. If you still reading this, you are in love with this design. <a href="blog-4.html">Read more...</a> </p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-6 col-xl-4 col-xxl-4 mb-4 position-relative ">-->
                    <!-- offer block-->
                   <!-- <div class="card border-0 position-relative overflow-hidden mb-4">


                        <figure class="coverimg position-absolute w-100 h-100 start-0 top-0 m-0">
                            <img src="assets/img/bg-13.jpg" class="mw-100" alt="" />
                        </figure>
                        <div class="card-body bg-none">
                            <div class="row">
                                <div class="col-7">
                                    <div class="bg-theme text-white text-center p-3 rounded">
                                        <h3>15% OFF</h3>
                                        <p>Holiday Trip</p>
                                        <p class="text-muted small">Price including with our launch offer get 5% Extra</p>
                                        <button class="copy-text btn btn-sm btn-rounded btn-outline-dashed text-white border-white">GOWFAMILY</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-0 overflow-hidden">
                        <div class="row mx-0">
                            <div class="col-6 pe-0 bg-theme text-white half-circle-vertical text-center py-4 z-index-1">
                                <div class="position-relative">
                                    <h3>15% OFF</h3>
                                    <p>Uberazia Taxi</p>
                                    <p class="text-muted small">Price including with our launch offer get 5% Extra</p>
                                    <button class="copy-text btn btn-sm btn-rounded btn-outline-dashed text-white border-white">GOREELLAUNCH</button>
                                </div>
                            </div>
                            <div class="col-6 position-relative">
                                <figure class="coverimg position-absolute w-100 h-100 start-0 top-0 m-0">
                                    <img src="assets/img/news-3.jpg" class="mw-100" alt="" />
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-6 col-xl-4 col-xxl-4 mb-4 ">-->
                    <!-- photo gallery view -->
                    <!--<div class="card border-0 overflow-hidden h-100">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-auto">
                                    <i class="bi bi-image h5 me-1 avatar avatar-40 bg-light-purple text-purple rounded me-2"></i>
                                </div>
                                <div class="col ps-0">
                                    <h6 class="fw-medium mb-0">Photo Gallery</h6>
                                    <p class="text-secondary small">Click image to see preview</p>
                                </div>
                                <div class="col-auto">
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-0 gallery mnh-350">
                            <div class="row g-0 h-100">
                                <div class="col-6 h-100 position-relative overflow-hidden">
                                    <a href="assets/img/bg-8.jpg" class="coverimg position-absolute w-100 h-100 start-0 top-0 m-0 zoomout">
                                        <img src="assets/img/bg-8.jpg" class="mw-100" alt="" title="Love unconditional when it comes to your own" />
                                    </a>
                                </div>
                                <div class="col-6 h-100">
                                    <div class="row h-100">
                                        <div class="col-12 h-50 position-relative overflow-hidden">
                                            <a href="assets/img/business-4.jpg" class="coverimg position-absolute w-100 h-100 start-0 top-0 m-0 zoomout">
                                                <img src="assets/img/business-4.jpg" class="mw-100" alt="" title="Your business your strength" />
                                            </a>
                                        </div>
                                        <div class="col-12 h-50 position-relative overflow-hidden">
                                            <a href="assets/img/bg-2.jpg" class="coverimg position-absolute w-100 h-100 start-0 top-0 m-0 zoomout">
                                                <img src="assets/img/bg-2.jpg" class="mw-100" alt="" title="Beautiful background image" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div> -->