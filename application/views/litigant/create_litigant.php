<main class="main mainheight">
  <div class="container">
    <!-- forms -->
    <h5 class="title"><?=$page_title?></h5>
    <form action="<?=base_url('Litigant/store')?>" method="post" id="litigantForm">
      <div class="row">
        <div class="col-12 col-md-6 col-xl-4 mb-4 mb-md-0 ">
          <!-- was-validated class on form when data are submitted -->
          <div class="mb-4">
            <div class="form-group mb-3 position-relative check-valid is-invalid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                <div class="form-floating">
                  <input type="text" placeholder="कम्प्यूटरीकृत वाद संख्या" name="dispute_no" id="dispute_no"
                    class="form-control border-start-0">
                  <label>कम्प्यूटरीकृत वाद संख्या</label>
                </div>
              </div>
            </div>
          </div>
          <div class="mb-4">
            <div class="form-group mb-3 position-relative">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                <div class="form-floating">
                  <select class="form-select border-0" name="block" id="block" onclick="getVillage(this.value)">
                    <option value=""> Select Block</option>
                    <?php foreach($blocks as $block){?>
                    <option value="<?=$block->id?>"> <?=$block->name?></option>
                    <?php } ?>
                  </select>
                  <label>ब्लॉक</label>
                </div>
              </div>
            </div>
          </div>

          <div>
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0"><i class=" "></i></span>
                <div class="form-floating">
                  <input type="date" placeholder="Enter Admission Dsate" name="admission_date" id="admission_date"
                    class="form-control border-start-0">
                  <label>दाखिला तिथि</label>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                <div class="form-floating">
                  <input type="text" placeholder="Enter scheduled action" name="scheduled_action" id="scheduled_action"
                    class="form-control border-start-0">
                  <label>अपेछित कार्यवाही</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-6 col-xl-4 mb-4 mb-md-0">
          <!-- Form elements -->

          <div class="mb-4">
            <div class="form-group mb-3 position-relative">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                <div class="form-floating">
                  <select class="form-select border-0" name="section" id="section">
                    <option value=""> Select Section</option>
                    <?php foreach($sections as $section){?>
                    <option value="<?=$section->id?>"> <?=$section->name?></option>
                    <?php } ?>
                  </select>
                  <label>सेक्शन</label>
                </div>
              </div>
            </div>
          </div>

          <div class="mb-4">
            <div class="form-group mb-3 position-relative">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                <div class="form-floating">
                  <select class="form-select border-0" name="village_name" id="village_name">
                    <option value=""> Select Village(Gram)</option>

                  </select>
                  <label>ग्राम</label>
                </div>
              </div>
            </div>
          </div>
          <div class="mb-4">
            <div class="form-group mb-3 position-relative check-valid is-invalid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                <div class="form-floating">
                  <input type="text" placeholder="Enter Old dispute no" name="plaintiffs" id="plaintiffs"
                     class="form-control border-start-0">
                  <label>वादी</label>
                </div>
              </div>
            </div>
          </div>


        </div>
        <div class="col-12 col-md-6 col-xl-4 mb-4 mb-md-0">
          <div class="mb-4">
            <div class="form-group mb-3 position-relative">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                <div class="form-floating">
                  <select class="form-select border-0" name="acts" id="acts">
                    <option value=""> Select Acts</option>
                    <?php foreach($acts as $act){?>
                    <option value="<?=$act->id?>"> <?=$act->name?></option>
                    <?php } ?>
                  </select>
                  <label>एक्ट्स</label>
                </div>
              </div>
            </div>
          </div>

          <div class="mb-2">
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                <div class="form-floating">
                  <input type="text" placeholder="Enter Police Station" name="police_station" id="police_station"
                     class="form-control border-start-0">
                  <label>थाना</label>
                </div>
              </div>
            </div>
          </div>
          <div class="mb-2">
            <div class="form-group mb-3 position-relative check-valid is-invalid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0"><i class=""></i></span>
                <div class="form-floating">
                  <input type="text" placeholder="प्रतिवादी"  name="defendants"
                    id="defendants" class="form-control border-start-0">
                  <label>प्रतिवादी</label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-12 col-xl-12 mb-12 mb-md-0 text-center">
          <button type="submit" class="btn btn-theme">Submit</button>
        </div>

      </div>
    </form>
  </div>

  <script>
  $("form#litigantForm").submit(function(e) {
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
      success: function(data) {
        if (data.status == 200) {
          toastr.success(data.message);
          $(':input[type="submit"]').prop('disabled', false);
          setTimeout(function() {
            location.href = "<?=base_url('litigant')?>";
          }, 1000)

        } else if (data.status == 403) {
          toastr.error(data.message);
          $(':input[type="submit"]').prop('disabled', false);
        } else {
          toastr.error('Something went wrong');
          $(':input[type="submit"]').prop('disabled', false);
        }
      },
      error: function() {}
    });
  });

  function getVillage(value) {
    $.ajax({
      url: '<?=base_url('Ajax_controller/getVillage')?>',
      type: 'POST',
      data: {
        value
      },
      success: function(data) {
        $('#village_name').html(data);
      }
    });
  }
  </script>