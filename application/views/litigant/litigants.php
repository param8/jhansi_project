<main class="main mainheight">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 position-relative ">
        <!-- Grid table-->
        <div class="card border-0 mb-4">
          <div class="card-header">
            <div class="row">
              <div class="col-auto mb-2">
                <i class="bi bi-shop h5 avatar avatar-40 bg-light-theme rounded"></i>
              </div>
              <div class="col align-self-center mb-2">
                <h6 class="d-inline-block mb-0"><?=$page_title?></h6>
              </div>
              <?php if($this->session->userdata('user_type')=='व्यवस्थापक' || $this->session->userdata('user_type')=='उपयोगकर्ता'){?>
              <div class="col-12 col-sm-auto mb-2">
                <div class="rounded">
                  <span><a href="<?=base_url('create-litigant')?>" class="btn btn-primary"><?=$page_title?> जोड़ें </a>
                  </span>
                  <a type="button" data-toggle="modal" data-target="#bulkLitigantsModal" class="btn btn-primary">सामूहिक
                    अपलोड</a>
                  <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addVillageModal">Add Village
                </button> -->
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <form action="<?=base_url('Authantication/searchSessionSet')?>" id="searchForm">
                <div class="input-group p-2">
                  <div class="inp_ut">
                    <input type="text" class="form-control" id="disputeNo" name="disputeNo"
                      value="<?=$this->session->userdata('disputeNo')?>" placeholder="कम्प्यूटरीकृत वाद संख्या"
                      autocomplete="off">
                    <span class="s_data" id="coursesData" style="display:none"></span>
                  </div>
                  <div class="inp_ut">
                    <input type="text" onfocus="(this.type='date')" class="form-control" name="start_date"
                      id="start_date" value="<?=$this->session->userdata('start_date')?>"
                      onchange="endDateValidate(this.value)" placeholder="दाखिला तिथि से">
                    <!-- <input type="text" onfocus="(this.type='date')" min="<?= date('Y-m-d'); ?>" class="form-control" id="start_date" name="end_date" placeholder="दाखिला तिथि से"> -->
                  </div>
                  <div class="inp_ut" id="endDate_div">
                    <!-- <input type="text" onfocus="(this.type='date')" min="<?= date('Y-m-d'); ?>" class="form-control" id="start_date" name="end_date" placeholder="दाखिला तिथि तक"> -->
                    <input type="text" class="form-control" value="<?=$this->session->userdata('end_date')?>"
                      placeholder="दाखिला तिथि तक">
                  </div>
                  <div class="inp_ut">
                    <select type="text" class="form-control" id="block" name="block" onchange="getVillage(this.value)"
                      placeholder="विकास खण्ड चुनें">
                      <option value=""> विकास खण्ड चुनें</option>
                      <?php foreach($blocks as $block){
                      ?>
                      <option value="<?=$block->id?>"
                        <?=$block->id == $this->session->userdata('block') ? 'selected' : ''?>> <?=$block->name?>
                      </option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="inp_ut">
                    <select type="text" class="form-control" id="village" name="village" placeholder="ग्राम चुनें">
                      <option value=""> ग्राम चुनें</option>
                    </select>
                  </div>
                  <div class="inp_ut">
                    <button class="btn btn-primary" name="submit" type="submit">खोजें <i
                        class="bi bi-search"></i></button>
                  </div>
                  <div class="inp_ut">
                    <button class="btn btn-danger" name="submit" type="button" onclick="SearchSessionUnset()"><i
                        class="bi bi-bootstrap-reboot"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="card-body p-3">
            <div class="table table-responsive ">
              <table class="table table-striped table-bordered  table-sm" style="width:100%" id="litigantDatatable">
                <thead>
                  <tr>
                    <th>क्रमांक</th>
                    <?php if($this->session->userdata('user_type')=='उपयोगकर्ता' || $this->session->userdata('user_type')=='व्यवस्थापक'){?>
                    <th>कार्य</th>
                    <?php } ?>
                    <th nowrap>आख्या पर टिप्पड़ी</th>
                    <th nowrap>कम्प्यूटरीकृत वाद संख्या</th>
                    <th nowrap>सेक्शन</th>
                    <th nowrap>एक्ट</th>
                    <th nowrap>ब्लॉक</th>
                    <th nowrap>ग्राम का नाम</th>
                    <th>थाना</th>
                    <th nowrap>दाखिला तिथि</th>
                    <th nowrap>वादी </th>
                    <th nowrap>प्रतिवादी</th>
                    <th nowrap>अपेछित कार्यवाही</th>

                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer bg-none">
            <div class="row align-items-center text-center">
              <div class=" col-12 mb-2 ">
                <span class=" hide-if-no-paging">
                  Showing <span id="footablestot1"></span> page
                </span>
              </div>
              <div class="col-12" id="footable-pagination1"></div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- Bulk Upload Modal start-->
    <div class="modal" id="bulkLitigantsModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="<?=base_url('Litigant/bulkStore')?>" method="post" id="litigantExelBulkUploadForm">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">सामूहिक अपलोड</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
              <div class="mb-2">
                <div class="form-group mb-3 position-relative check-valid">
                  <div class="input-group input-group-lg">
                    <span class="input-group-text text-theme border-end-0">File:</span>
                    <div class="form-floating">
                      <input type="file" placeholder="Litigant File" name="litigantExel" id="litigantExel"
                        class="form-control border-start-0">
                      <label>फ़ाइल अपलोड करें</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
              <a href="<?=base_url('Litigant/download_sample')?>" class="btn btn-warning">Sample Download</a>
              <button type="submit" class="btn btn-primary">जमा करें</button>

            </div>
          </form>
        </div>
      </div>
    </div>
    <!--Bulk Upload Modal End -->
    <!-- Litigant remark Modal start-->
    <div class="modal" id="litigantRemarkModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="<?=base_url('Litigant/litigantReport')?>" method="post" id="litigantRemarkForm">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">वाद रिपोर्ट</h4>
              <button type="button" class="close" data-dismiss="modal" onclick="closwModal()">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body" id="viewDhyaData">

            </div>
        
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">जमा करें</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--Litigant remark Modal End -->
    <!-- Litigant View Modal start-->
    <div class="modal" id="litigantViewkModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">वाद रिपोर्ट</h4>
            <button type="button" class="close" data-dismiss="modal" onclick="closeviewModal()">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div id="viewLitigant">

            </div>
          </div>
          <!-- Modal footer -->
        </div>
      </div>
    </div>
    <!--Litigant View Modal End -->

    <!-- Active Status Modal -->
    <div class="modal" id="ActiveLitigantsModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="<?=base_url('Litigant/activeLitigants')?>" method="post" id="activeLitigantsForm">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">सक्रिय करे </h4>
              <button type="button" class="close" data-dismiss="modal" onclick="closeviewModal()">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
            <div class="mb-2">
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0">टिप्पणी:</span>
                <div class="form-floating">
                  <textarea type="text"  name="acremark"  id="acremark" class="form-control border-start-0"></textarea>
                  <!-- <label>टिप्पणी</label> -->
                </div>
              </div>
            </div>
          </div>
          <div class="mb-2">
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0">File:</span>
                <div class="form-floating">
                  <input type="file" placeholder="फ़ाइल अपलोड करें" name="image" id="image" accept="application/pdf" class="form-control border-start-0">
                  <label>फ़ाइल अपलोड करें</label>
                </div>
              </div>
              <input type="hidden" name="id" id="activeID">
            </div>
          </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">जमा करें</button>

            </div>
          </form>
        </div>
      </div>
    </div>


    <!-- Deactive Status -->

     <!-- Active Status Modal -->
     <div class="modal" id="deactiveLitigantsModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="<?=base_url('Litigant/deactiveLitigants')?>" method="post" id="deactiveLitigantsForm">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">निष्क्रिय का कारन </h4>
              <button type="button" class="close" data-dismiss="modal" onclick="closeviewModal()">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
            <div class="mb-2">
            <div class="form-group mb-3 position-relative check-valid">
              <div class="input-group input-group-lg">
                <span class="input-group-text text-theme border-end-0">निष्क्रिय का कारन  :</span>
                <div class="form-floating">
                  <textarea type="text"  name="deremark"  id="deremark" class="form-control border-start-0"></textarea>
                  <!-- <label>टिप्पणी</label> -->
                </div>
              </div>
            </div>
            <input type="hidden" name="id" id="deactiveID">
          </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
  
              <button type="submit" class="btn btn-primary">जमा करें</button>

            </div>
          </form>
        </div>
      </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function() {
      // $('#example').DataTable();
      // } );
      var dataTable = $('#litigantDatatable').DataTable({
        "processing": true,
        "serverSide": true,
        buttons: [{
          extend: 'excelHtml5',
          text: 'Download Excel'
        }],
        "order": [],
        "ajax": {
          url: "<?=base_url('Litigant/ajaxLitigant')?>",
          type: "POST"
        },
        "columnDefs": [{
          "targets": [0],
          "orderable": false,
        }, ],
      });
    });


    function getVillageModal(id) {
      //alert(id);
      $.ajax({
        url: '<?=base_url('Village/get_village')?>',
        type: 'POST',
        data: {
          id
        },
        success: function(data) {
          $('#editVillageModal').modal('show');
          $('#editFormData').html(data);
        }
      });
    }


    $("form#villageBlockForm").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();
      var formData = new FormData(this);
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastr.success(data.message);
            $(':input[type="submit"]').prop('disabled', false);
            setTimeout(function() {
              location.href = "<?=base_url('Village')?>";
            }, 1000)

          } else if (data.status == 403) {
            toastr.error(data.message);
            $(':input[type="submit"]').prop('disabled', false);
          } else {
            toastr.error('Something went wrong');
            $(':input[type="submit"]').prop('disabled', false);
          }
        },
        error: function() {}
      });
    });

    $("form#editVillageForm").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();
      var formData = new FormData(this);
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastr.success(data.message);
            $(':input[type="submit"]').prop('disabled', false);
            setTimeout(function() {
              location.href = "<?=base_url('Village')?>";
            }, 1000)

          } else if (data.status == 403) {
            toastr.error(data.message);
            $(':input[type="submit"]').prop('disabled', false);
          } else {
            toastr.error('Something went wrong');
            $(':input[type="submit"]').prop('disabled', false);
          }
        },
        error: function() {}
      });
    });


    function deleteLitigant(id) {
      var messageText = "आप इस वाद को हटाना चाहते हैं?";
      var confirmText = 'हाँ, इसे बदलो!';
      var message = "सफलतापूर्वक वाद हट गया!";
      Swal.fire({
        title: 'क्या आपको यकीन है?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            url: '<?=base_url('litigant/delete')?>',
            method: 'POST',
            data: {
              id
            },
            success: function(result) {
              toastr.success(message);
              setTimeout(function() {
                window.location.reload();
              }, 2000);
            }

          });

        } else {
          location.reload();
        }
      })
    }

    $("form#litigantExelBulkUploadForm").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();
      var formData = new FormData(this);
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastr.success(data.message);
            $(':input[type="submit"]').prop('disabled', false);
            setTimeout(function() {
              location.href = "<?=base_url('Litigant')?>";
            }, 1000)

          } else if (data.status == 403) {
            toastr.error(data.message);
            $(':input[type="submit"]').prop('disabled', false);
          } else {
            toastr.error('Something went wrong');
            $(':input[type="submit"]').prop('disabled', false);
          }
        },
        error: function() {}
      });
    });

    $("form#litigantRemarkForm").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();
      var formData = new FormData(this);
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastr.success(data.message);
            $(':input[type="submit"]').prop('disabled', false);
            setTimeout(function() {
              location.href = "<?=base_url('Litigant')?>";
            }, 1000)

          } else if (data.status == 403) {
            toastr.error(data.message);
            $(':input[type="submit"]').prop('disabled', false);
          } else {
            toastr.error('Something went wrong');
            $(':input[type="submit"]').prop('disabled', false);
          }
        },
        error: function() {}
      });
    });

    function getVillage(value) {
      $.ajax({
        url: '<?=base_url("Ajax_controller/getVillage")?>',
        type: 'POST',
        data: {
          value
        },
        success: function(data) {
          $('#village').html(data);
        },
      });
    }

    $(document).ready(function() {
      getVillage(<?=$this->session->userdata('block')?>);
    });

    function endDateValidate(start_date) {
      $('#endDate_div').html(' <input type="date" class="form-control" min="' + start_date +
        '" onchange="get_duration(this.value)" name="end_date" id="end_date" placeholder="दाखिला तिथि तक">');
    }

    $("form#searchForm").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();
      var formData = new FormData(this);
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(data) {
          location.href = "<?=base_url('Litigant')?>";
        },
        error: function() {}
      });
    });


    function SearchSessionUnset() {
      $.ajax({
        url: '<?=base_url("Authantication/sessionDestroy")?>',
        type: 'POST',
        success: function(data) {
          location.href = "<?=base_url('Litigant')?>";
        },
      });
    }

    function getLitigantModal(id) {
      $.ajax({
        url: '<?=base_url('Litigant/get_litigant_edit')?>',
        type: 'POST',
        data: {
          id
        },
        success: function(data) {
          $('#litigantRemarkModal').modal('show');
          $('#viewDhyaData').html(data);
        }
      });

    }

    function closwModal() {
      $('#litigantRemarkModal').modal('hide');
    }

    function viewLitigantModal(id) {
      $.ajax({
        url: '<?=base_url('Litigant/get_litigant')?>',
        type: 'POST',
        data: {
          id
        },
        success: function(data) {
          $('#litigantViewkModal').modal('show');
          $('#viewLitigant').html(data);
        }
      });
    }

    function closeviewModal() {
      $('#litigantViewkModal').modal('hide');
    }

    function getStatusApproveModal(id){
      $('#ActiveLitigantsModal').modal('show');   
      $('#activeID').val(id)  
    }

    function getStatusDisapproveModal(id){
      $('#deactiveLitigantsModal').modal('show');   
      $('#deactiveID').val(id)
    }

    function closeviewModal() {
      $('#ActiveLitigantsModal').modal('hide');
      $('#litigantViewkModal').modal('hide');
      $('#deactiveLitigantsModal').modal('hide');  
    }


    $("form#activeLitigantsForm").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();
      var formData = new FormData(this);
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastr.success(data.message);
            $(':input[type="submit"]').prop('disabled', false);
            setTimeout(function() {
              location.href = "<?=base_url('Litigant')?>";
            }, 1000)

          } else if (data.status == 403) {
            toastr.error(data.message);
            $(':input[type="submit"]').prop('disabled', false);
          } else {
            toastr.error('Something went wrong');
            $(':input[type="submit"]').prop('disabled', false);
          }
        },
        error: function() {}
      });
    });

    $("form#deactiveLitigantsForm").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();
      var formData = new FormData(this);
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
          if (data.status == 200) {
            toastr.success(data.message);
            $(':input[type="submit"]').prop('disabled', false);
            setTimeout(function() {
              location.href = "<?=base_url('Litigant')?>";
            }, 1000)

          } else if (data.status == 403) {
            toastr.error(data.message);
            $(':input[type="submit"]').prop('disabled', false);
          } else {
            toastr.error('Something went wrong');
            $(':input[type="submit"]').prop('disabled', false);
          }
        },
        error: function() {}
      });
    });
    </script>