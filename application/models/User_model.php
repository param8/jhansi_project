<?php 

class User_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

	public function get_all_users($condition)
	{
		$this->db->select('users.*,states.name as stateName,cities.city as cityName');
		$this->db->from('users');
		$this->db->join('states','states.id=users.state','left');
		$this->db->join('cities','cities.id=users.city','left');
		$this->db->where('users.id<>',1);
		$this->db->where($condition);
	    $this->db->order_by('users.id','asc');
		return $this->db->get()->result();
	}

	public function get_user($condition){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where($condition);
		$query =  $this->db->get()->row();
		return $query;
	}

	public function update($data,$id)
	{
		 $this->db->where($id); 
		return $this->db->update('users',$data);
	}

	
	public function delete($id)
	{
		 $this->db->where($id); 
		return $this->db->delete('users');
	}

	public function get_user_details($data){
		$this->db->select('users.*,states.name as stateName,cities.city as cityName');
		$this->db->join('states','states.id=users.state','left');
		$this->db->join('cities','cities.id=users.city','left');
		$this->db->where($data);
		$result =  $this->db->get('users');
			return $result->row();	
	}
	
	public function stor_enquiry($data){
		return $this->db->insert('contact_us', $data);
	}

  public function store($data){
    return $this->db->insert('users', $data);
  }



}