<?php 
class Setting_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function update_siteInfo($data){
    $adminID =  $this->session->userdata('id');
    $this->db->where('adminID', $adminID);
    return $this->db->update('site_info',$data);
    //echo $this->db->last_query();die;
  }

  public function store_siteInfo($data){
    return $this->db->insert('site_info',$data);
  }

  public function get_sections($condition){
     $this->db->where($condition);
     return $this->db->get('sections')->result();
  }

  public function get_section($condition){
    $this->db->select('*');
    $this->db->from('sections');
    $this->db->where($condition);
    return $this->db->get()->row();
    }


    public function store_section($data){
      $this->db->insert('sections',$data);
     return $this->db->insert_id();
    }

    public function update_section($data, $id){
      $this->db->where('id',$id);
      return $this->db->update('sections',$data);
    }

    public function get_acts($condition){
      $this->db->where($condition);
      return $this->db->get('acts')->result();
   }
 
   public function get_act($condition){
     $this->db->select('*');
     $this->db->from('acts');
     $this->db->where($condition);
     return $this->db->get()->row();
     }
 
 
     public function store_acts($data){
       $this->db->insert('acts',$data);
      return $this->db->insert_id();
     }
 
     public function update_acts($data, $id){
       $this->db->where('id',$id);
       return $this->db->update('acts',$data);
     }
 

}