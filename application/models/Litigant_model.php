<?php 

class Litigant_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}


    public function store($data){
    return $this->db->insert('litigants',$data);

    }

    public function get_all_litigants($condition){
        $this->db->select('*');
		$this->db->from('litigants');
		$this->db->where($condition);
        if(!empty($this->session->userdata('disputeNo'))){
        $this->db->where('dispute_no', $this->session->userdata('disputeNo'));
        }
        if(!empty($this->session->userdata('start_date'))){
            $start_date = date('d/m/Y', strtotime($this->session->userdata('start_date')));
            $end_date = date('d/m/Y', strtotime($this->session->userdata('end_date')));
            $this->db->where("admission_date BETWEEN '$start_date' AND '$end_date'");
        }
        if(!empty($this->session->userdata('village'))){
            $this->db->where('village_name', $this->session->userdata('village'));
        }
	    $this->db->order_by('id','desc');
		return $this->db->get()->result();
         //echo $this->db->last_query(); die;
    }

    public function get_litigant($condition){
        $this->db->select('*');
		$this->db->from('litigants');
		$this->db->where($condition);
		return $this->db->get()->row();
        //echo $this->db->last_query();
    }

    public function update($data,$id){
        $this->db->where('id',$id);
       return $this->db->update('litigants',$data);
    }

    public function delete($id){
        $this->db->where('id',$id);
        return $this->db->delete('litigants');
    }

    public function store_remark($data){
      return $this->db->insert('litigant_remark',$data);
    }
    public function update_remark($data,$id){
     $this->db->where('litigantId',$id);
    return $this->db->update('litigant_remark',$data);
    }
    
    public function get_litigant_remark($id){
      $this->db->select('*');
		  $this->db->from('litigant_remark');
		  $this->db->where($id);
      return $this->db->get()->row();
    }


    function make_query($condition)
    {
     $this->db->select('litigants.*,sections.name as sectionName,acts.name as actName,block.name as blockName, village.name as villageName');
     $this->db->from('litigants');
     $this->db->join('sections','sections.id = litigants.section','left');
     $this->db->join('acts','acts.id = litigants.acts','left');
     $this->db->join('block','block.id = litigants.block','left');
     $this->db->join('village','village.id = litigants.village_name','left');
     $this->db->where($condition);
     if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
     {
      $this->db->like('sections.name', $_POST["search"]["value"]);
      $this->db->or_like('acts.name', $_POST["search"]["value"]);
      $this->db->or_like('block.name', $_POST["search"]["value"]);
      $this->db->or_like('village.name', $_POST["search"]["value"]);
      $this->db->or_like('litigants.dispute_no', $_POST["search"]["value"]);
      $this->db->or_like('litigants.police_station', $_POST["search"]["value"]);
      $this->db->or_like('litigants.plaintiffs', $_POST["search"]["value"]);
      $this->db->or_like('litigants.defendants', $_POST["search"]["value"]);
      $this->db->or_like('litigants.scheduled_action', $_POST["search"]["value"]);

     }
     $this->db->order_by('litigants.id','desc');   
    }
  
  
      function make_datatables($condition){
          $this->make_query($condition,);
          if($_POST["length"] != -1)
          {
              $this->db->limit($_POST['length'], $_POST['start']);
          }
          $query = $this->db->get();
          return $query->result_array();
          
      }
  
      function get_all_data($condition)
      {
        $this->db->select('litigants.*,sections.name as sectionName,acts.name as actName,block.name as blockName, village.name as villageName');
        $this->db->from('litigants');
        $this->db->join('sections','sections.id = litigants.section','left');
        $this->db->join('acts','acts.id = litigants.acts','left');
        $this->db->join('block','block.id = litigants.block','left');
        $this->db->join('village','village.id = litigants.village_name','left');
        $this->db->where($condition);
        if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
        {
          $this->db->like('sections.name', $_POST["search"]["value"]);
          $this->db->or_like('acts.name', $_POST["search"]["value"]);
          $this->db->or_like('block.name', $_POST["search"]["value"]);
          $this->db->or_like('village.name', $_POST["search"]["value"]);
          $this->db->or_like('litigants.dispute_no', $_POST["search"]["value"]);
          $this->db->or_like('litigants.police_station', $_POST["search"]["value"]);
          $this->db->or_like('litigants.plaintiffs', $_POST["search"]["value"]);
          $this->db->or_like('litigants.defendants', $_POST["search"]["value"]);
          $this->db->or_like('litigants.scheduled_action', $_POST["search"]["value"]);
        }
        $this->db->order_by('litigants.id','desc');   
        return $this->db->count_all_results();
      }

      function get_filtered_data($condition){
          $this->make_query($condition);
          $query = $this->db->get();
          return $query->num_rows();
          //echo $this->db->last_query();die;
      }


}