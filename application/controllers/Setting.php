<?php 
class Setting extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
    $this->load->model('setting_model');
	}

    public function index(){
     $data['page_title'] = 'सेक्शन';
     $data['sections'] = $this->setting_model->get_sections(array('status' => 1));
		 $this->admin_template('setting/section',$data);
	  }

    public function store_section(){
       $name = $this->input->post('name');
       $section = $this->setting_model->get_section(array('name' => $name));
        if(empty($name)){
            echo json_encode(['status'=>403, 'message'=>'Please enter Section name']); 	
            exit();
        }

        if($section){
          echo json_encode(['status'=>403, 'message'=>'This section is already in use']); 	
            exit();
        }

        $data = array(
            'name'        => $name,
        );
        $register = $this->setting_model->store_section($data);
    
        if($register){
            echo json_encode(['status'=>200, 'message'=>'Section Add successfully!']);
        }else{
            echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
        }
    
        
    }

    public function get_section_form(){
        $id = base64_decode($this->input->post('id')); 
        $section = $this->setting_model->get_section(array('id' => $id));
        ?>
        <div class="form-group">
        <label for="name" class="col-form-label">Block:</label>
        <input type="text"  name="name" id="name" value="<?=$section->name?>" class="form-control border-start-0">
      </div>
   
      <input type="hidden" name="id" id="id" value="<?=$id?>">
   
   <?php }

public function update_section(){
    $id = $this->input->post('id');
    $name = $this->input->post('name');
    $section = $this->setting_model->get_section(array('id<>'=>$id,'name' => $name));
    if(empty($name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter Section name']); 	
        exit();
    }

    $data = array(
        'name'        => $name,
    );
    $update = $this->setting_model->update_section($data, $id);

    if($update){
        echo json_encode(['status'=>200, 'message'=>'Section Update successfully!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
    }  
}

// public function delete(){
//     $id = $this->input->post('id'); 
//     $delete = $this->block_model->delete($id);

//     if($delete){
//         echo json_encode(['status'=>200, 'message'=>'Block Delete successfully!']);
//     }else{
//         echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
//     }  
// }



public function acts(){
  $data['page_title'] = 'एक्ट्स';
  $data['acts'] = $this->setting_model->get_acts(array('status' => 1));
  $this->admin_template('setting/acts',$data);
 }

 public function store_acts(){
    $name = $this->input->post('name');
    $section = $this->setting_model->get_act(array('name' => $name));
     if(empty($name)){
         echo json_encode(['status'=>403, 'message'=>'Please enter act name']); 	
         exit();
     }

     if($section){
       echo json_encode(['status'=>403, 'message'=>'This act is already in use']); 	
         exit();
     }

     $data = array(
         'name'        => $name,
     );
     $register = $this->setting_model->store_acts($data);
 
     if($register){
         echo json_encode(['status'=>200, 'message'=>'Act Add successfully!']);
     }else{
         echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
     }
 
     
 }

 public function get_acts_form(){
     $id = base64_decode($this->input->post('id')); 
     $act = $this->setting_model->get_act(array('id' => $id));
     ?>
     <div class="form-group">
     <label for="name" class="col-form-label">Acts:</label>
     <input type="text"  name="name" id="name" value="<?=$act->name?>" class="form-control border-start-0">
   </div>

   <input type="hidden" name="id" id="id" value="<?=$id?>">

<?php }

public function update_acts(){
 $id = $this->input->post('id');
 $name = $this->input->post('name');
 $section = $this->setting_model->get_act(array('id<>'=>$id,'name' => $name));
 if(empty($name)){
     echo json_encode(['status'=>403, 'message'=>'Please enter acts name']); 	
     exit();
 }

 $data = array(
     'name'        => $name,
 );
 $update = $this->setting_model->update_acts($data, $id);

 if($update){
     echo json_encode(['status'=>200, 'message'=>'Acts Update successfully!']);
 }else{
     echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
 }  
}

// public function delete(){
//  $id = $this->input->post('id'); 
//  $delete = $this->block_model->delete($id);

//  if($delete){
//      echo json_encode(['status'=>200, 'message'=>'Block Delete successfully!']);
//  }else{
//      echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
//  }  
// }



}