<?php 
class Litigant extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('village_model');
        $this->load->model('litigant_model');
        $this->load->model('block_model');
        $this->load->model('setting_model');
        $this->load->library('Csvimport');
	}

    public function index(){
        $data['page_title'] = 'वाद सूची';
        $data['litigants'] = $this->litigant_model->get_all_litigants(array('status' => 1));
        $data['blocks'] = $this->block_model->get_all_blocks(array('status' => 1));
        //print_r($data['litigants']);
        $this->session->unset_userdata('disputeNo');
        $this->session->unset_userdata('start_date');
        $this->session->unset_userdata('end_date');
        $this->session->unset_userdata('block');
        $this->session->unset_userdata('village');
        $this->admin_template('litigant/litigants',$data);
        }

        public function ajaxLitigant(){
            $this->not_admin_logged_in();
            $condition = array('litigants.status'=>1);
            $litigants = $this->litigant_model->make_datatables($condition); // this will call modal function for fetching data
            $data = array();
            foreach($litigants as $key=>$litigant) // Loop over the data fetched and store them in array
            {
                $button = '';
                $button_remark = '';
                $sub_array = array();
                if($this->session->userdata('user_type')=='उपयोगकर्ता' || $this->session->userdata('user_type')=='व्यवस्थापक'){
                $button .= '<a href="'.base_url('edit-litigant/'.base64_encode($litigant['id'])).'" data-toggle="tooltip" title="वाद संपादन"><i class="bi bi-pencil-square"></i></a>';
                $button .= ' <a class="text-danger" href="javascript:void(0)" onclick="deleteLitigant('.$litigant['id'].')" title="वाद मिटायें"><i class="bi bi-trash"></i></a>';	
                }
                
                if($this->session->userdata('user_type')=='उपयोगकर्ता' || $this->session->userdata('user_type')=='व्यवस्थापक'){
                  if($litigant['approve_status']!=0){
                    $akhya_color = $litigant['approve_status'] == 3 ?'btn-danger' :($litigant['approve_status'] == 1 ? 'btn-success' :'btn-warning');
                    $akhya_text = $litigant['approve_status'] == 3 ?'आख्या निष्क्रिय' :($litigant['approve_status'] == 1 ? 'आख्या सक्रिय' :'आख्या देखें ');
                   $button_remark .= '<a href="javascript:void(0)" class="btn '.$akhya_color.'" data-toggle="modal" onclick="viewLitigantModal('.$litigant['id'].')" title="टिप्पड़ी देखें ">'.$akhya_text.' <i class="bi bi-eye"></i></a>';
                //    '<a href="javascript:void(0)" class="btn btn-success" onclick="getStatusApproveModal('.$litigant['id'].')" data-toggle="modal" title="वाद पर टिप्पणी">वादी को मंजूरी दें<i class="text-success bi bi-check-circle h5"></i></a>';
                //    $button_remark .=  '<a href="javascript:void(0)" class="btn btn-danger" onclick="getStatusDisapproveModal('.$litigant['id'].')" data-toggle="modal" title="वाद पर टिप्पणी">वादी को अस्वीकार करें<i class="bi-file-plus"></i></a>';
                  }
                }
                if($this->session->userdata('user_type')=='लेखपाल'){
                  if($litigant['approve_status']==0 ){
                    $button_remark .= '<a href="javascript:void(0)" class="btn btn-primary" onclick="getLitigantModal('.$litigant['id'].')" data-toggle="modal" title="वाद पर टिप्पणी">आख्या <i class="bi-file-plus"></i></a>';
                    }
                    if($litigant['approve_status']!=0){
                        $btn_color1 = $litigant['approve_status']==2 || $litigant['approve_status']==3 ? 'btn-primary' : ($litigant['approve_status']==1 ? 'btn-success' : '');
                        $commnent1 = $litigant['approve_status']==2 || $litigant['approve_status']==3 ? 'आख्या' : ($litigant['approve_status']==1 ? 'आख्या सक्रिय ' : '');
                        $button_remark .=  '<a href="javascript:void(0)" class="btn '.$btn_color1.'" data-toggle="modal" onclick="viewLitigantModal('.$litigant['id'].')" title="टिप्पड़ी देखें ">'.$commnent1.' <i class="bi bi-eye"></i></a>';
                      if($litigant['approve_status']!=1){
                        $btn_color = $litigant['approve_status']== 2 ? 'btn-primary' : ($litigant['approve_status']==3 ? 'btn-danger' : '');
                        $commnent = $litigant['approve_status']==2 ? 'आख्या' : ($litigant['approve_status']==3 ? 'आख्या निष्क्रिय कर दी गयी है कृपया पुनः आख्या अपलोड करे' : '');
                        $button_remark .= '<a href="javascript:void(0)" class="btn '.$btn_color.' " onclick="getLitigantModal('.$litigant['id'].')" data-toggle="modal" title="वाद पर टिप्पणी">'.$commnent.' <i class="bi-pencil-square"></i></a>';
                    }
                }
                }
                
                $sub_array[] = $key+1;
                if($this->session->userdata('user_type')=='उपयोगकर्ता' || $this->session->userdata('user_type')=='व्यवस्थापक'){
                $sub_array[] = $button;
              } 
                $sub_array[] = $button_remark;
                $sub_array[] = $litigant['dispute_no'];
                $sub_array[] = $litigant['sectionName'];
                $sub_array[] = $litigant['actName'];
                $sub_array[] = $litigant['blockName'];
                $sub_array[] = $litigant['villageName'];
                $sub_array[] = $litigant['police_station'];
                $sub_array[] = $litigant['admission_date'];
                $sub_array[] = $litigant['plaintiffs'];
                $sub_array[] = $litigant['defendants'];
                $sub_array[] = $litigant['scheduled_action'];
                $sub_array[] = date('d-F-Y', strtotime($litigant['created_at']));
                $data[] = $sub_array;
            }
        
            $output = array(
                "draw"                    =>     intval($_POST["draw"]),
                "recordsTotal"            =>     $this->litigant_model->get_all_data($condition),
                "recordsFiltered"         =>     $this->litigant_model->get_filtered_data($condition),
                "data"                    =>     $data
            );
            
            echo json_encode($output);
        }

        public function create(){
            $data['page_title'] = 'वाद सूची जोड़ें';
            $data['sections'] = $this->setting_model->get_sections(array('status' => 1)); 
            $data['acts'] = $this->setting_model->get_acts(array('status' => 1)); 
            $data['blocks']   =   $this->block_model->get_all_blocks(array('status' => 1)); 
            $this->admin_template('litigant/create_litigant',$data);   
        }

    public function edit(){
        $data['page_title'] = 'एडिट वाद सूची';
        $id = base64_decode($this->uri->segment(2));
        $data['litigant'] = $this->litigant_model->get_litigant(array('id' => $id)); 
        $data['sections'] = $this->setting_model->get_sections(array('status' => 1)); 
        $data['acts'] = $this->setting_model->get_acts(array('status' => 1)); 
        $data['blocks']   =   $this->block_model->get_all_blocks(array('status' => 1)); 
        $this->admin_template('litigant/edit_litigant',$data);   
    }

    public function litigant_view(){
        $id = base64_decode($this->uri->segment(2));
        $data['page_title'] = 'वाद का सारांश';
        $data['litigant'] = $this->litigant_model->get_litigant(array('id' => $id)); 
        $this->admin_template('litigant/litigant_view',$data);   
    }

    

    function bulkStore(){
        ini_set('display_errors',1);
        if (isset($_FILES["litigantExel"])) {
            //  echo "hi";die;
            $config['upload_path']   = "uploads/csv/village";
            $config['allowed_types'] = 'text/plain|text/csv|csv';
            $config['max_size']      = '2048';
            $config['file_name']     = $_FILES["litigantExel"]['name'];
            $config['overwrite']     = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            //echo "hi";
            if (!$this->upload->do_upload("litigantExel")) {
            echo json_encode(['status'=>403, 'message'=>'Please Upload Valid CSV files']);
            }else{
            $file_data = $this->upload->data();
            //print_r($file_data);die;
            $file_path = 'uploads/csv/village/'.$file_data['file_name'];
            if ($this->csvimport->get_array($file_path)) {
                $csv_array = $this->csvimport->get_array($file_path); 
                
                foreach($csv_array as $key=>$value){
                    $litigant = $this->litigant_model->get_litigant(array('litigants.dispute_no'=>$value['कम्प्यूटरीकृत वाद संख्या']));
                    $block = $this->block_model->get_blocks(array('block.name'=>$value['ब्लॉक']));
                    $village = $this->village_model->get_village(array('village.name'=>$value['ग्राम']));
                  if(empty($value['कम्प्यूटरीकृत वाद संख्या'])){
                    echo json_encode(['status'=>403, 'message'=>'कृप्या कम्प्यूटरीकृत वाद संख्या लिखें !']); 	
                    exit();
                  }
                  if($litigant){
                    echo json_encode(['status'=>403, 'message'=>'यह कम्प्यूटरीकृत वाद संख्या पहले से उपलध है !']); 	
                    exit();
                }
                  if(empty($value['सेक्शन'])){
                    echo json_encode(['status'=>403, 'message'=>'कृप्या सेक्शन चयन करें !']); 	
                    exit();
                  }
                if(empty($value['एक्ट'])){
                    echo json_encode(['status'=>403, 'message'=>'कृप्या एक्ट चयन करें !']); 	
                    exit();
                }
           
                if(empty($value['ब्लॉक'])){
                    echo json_encode(['status'=>403, 'message'=>'कृप्या ब्लॉक चयन करें !']); 	
                    exit();
                }

                if(empty($block)){
                    echo json_encode(['status'=>403, 'message'=>'यह ब्लॉक उपलध नहीं  है !']); 	
                    exit();
                }
    
                if(empty($value['ग्राम'])){
                    echo json_encode(['status'=>403, 'message'=>'कृप्या ग्राम चयन करें !']); 	
                    exit();
                }

                if(empty($village)){
                    echo json_encode(['status'=>403, 'message'=>'यह ग्राम उपलध नहीं  है !']); 	
                    exit();
                } 

                if(empty($value['थाना'])){
                    echo json_encode(['status'=>403, 'message'=>'कृप्या थाना का नाम लिखें !']); 	
                    exit();
                }
                if(empty($value['दाखिला तिथि'])){
                    echo json_encode(['status'=>403, 'message'=>'कृप्या दाखिला तिथि चुनें !']); 	
                    exit();
                }
                if(empty($value['वादी'])){
                    echo json_encode(['status'=>403, 'message'=>'कृप्या वादी  लिखें !']); 	
                    exit();
                }
                if(empty($value['प्रतिवादी'])){
                    echo json_encode(['status'=>403, 'message'=>'कृप्या प्रतिवादी  लिखें !']); 	
                    exit();
                }
            }
                 
                foreach($csv_array as $key=>$row){
                
                    $section = $this->setting_model->get_section(array('sections.name'=>$row['सेक्शन']));
                    if($section){
                        $sectionID = $section->id;
                    }else{
                        $sectionData = array(
                          'name' =>$row['सेक्शन'],
                        );

                        $sectionID = $this->setting_model->store_section($sectionData);
                    }

                    $act = $this->setting_model->get_act(array('acts.name'=>$row['एक्ट']));
                    if($act){
                        $actID = $act->id;
                    }else{
                        $actData = array(
                          'name' => $row['एक्ट'],
                        );
                        $actID = $this->setting_model->store_acts($actData);
                    }
                    
                    $blocks = $this->block_model->get_blocks(array('block.name'=>$row['ब्लॉक']));
                    $blockID = $blocks->id;
                    $villages = $this->village_model->get_village(array('village.name'=>$row['ग्राम']));
                    $villageID = $villages->id;
                  
                  $data = array(
                    'userID'           => $this->session->userdata('id'),
                    'section'          => $sectionID,
                    'acts'             => $actID,
                    'dispute_no'       => $row['कम्प्यूटरीकृत वाद संख्या'],
                    'block'            => $blockID,
                    'village_name'     => $villageID,
                    'police_station'   => $row['थाना'],
                    'admission_date'   => date('Y-m-d',strtotime($row['दाखिला तिथि'])),
                    'plaintiffs'       => $row['वादी'],
                    'defendants'       => $row['प्रतिवादी'],
                    'scheduled_action' => $row['अपेछित कार्यवाही'],
                  );
                  $store = $this->litigant_model->store($data);
            }
                if($store){
                    echo json_encode(['status'=>200, 'message'=>'litigant Exel Upload successfully!']);
                }else{
                    echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
                }  
            } 
          }
         }
        }


        public function store(){
            $section = $this->input->post('section');
            $acts = $this->input->post('acts');
            $dispute_no = $this->input->post('dispute_no');
            $block = $this->input->post('block');
            $village_name = $this->input->post('village_name');
            $police_station = $this->input->post('police_station');
            $admission_date = $this->input->post('admission_date');
            $plaintiffs = $this->input->post('plaintiffs');
            $defendants = $this->input->post('defendants');
            $scheduled_action = $this->input->post('scheduled_action');

            $litigant = $this->litigant_model->get_litigant(array('litigants.dispute_no'=>$dispute_no));

            if(empty($dispute_no)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या कम्प्यूटरीकृत वाद संख्या लिखें !']); 	
                exit();
            }

            if($litigant){
                echo json_encode(['status'=>403, 'message'=>'यह कम्प्यूटरीकृत वाद संख्या पहले से उपलध है  !']); 	
                exit();
            }

            if(empty($section)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या सेक्शन चयन करें !']); 	
                exit();
            }
            if(empty($acts)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या एक्ट चयन करें !']); 	
                exit();
            }
       
            if(empty($block)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या ब्लॉक चयन करें !']); 	
                exit();
            }

            if(empty($village_name)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या ग्राम चयन करें !']); 	
                exit();
            }
            if(empty($police_station)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या थाना का नाम लिखें !']); 	
                exit();
            }
            if(empty($admission_date)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या दाखिला तिथि चुनें !']); 	
                exit();
            }
            if(empty($plaintiffs)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या वादी  लिखें !']); 	
                exit();
            }
            if(empty($defendants)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या प्रतिवादी  लिखें !']); 	
                exit();
            }
            if(empty($scheduled_action)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या अपेछित कार्यवाही लिखें !']); 	
                exit();
            }

            $data = array(
                'userID' => $this->session->userdata('id'),
                'section' => $section,
                'acts' => $acts,
                'dispute_no' => $dispute_no,
                'block' => $block,
                'village_name' => $village_name,
                'police_station' => $police_station,
                'admission_date' => $admission_date,
                'plaintiffs' => $plaintiffs,
                'defendants' => $defendants,
                'scheduled_action' => $scheduled_action,
            );

            $update = $this->litigant_model->store($data);
            if($update){
              echo json_encode(['status'=>200, 'message'=>'Litigant added successfully!']);
            }else{
                echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
            }
    }
    

        public function update(){
            $id = $this->input->post('id');
            $section = $this->input->post('section');
            $acts = $this->input->post('acts');
            $dispute_no = $this->input->post('dispute_no');
            $block = $this->input->post('block');
            $village_name = $this->input->post('village_name');
            $police_station = $this->input->post('police_station');
            $admission_date = $this->input->post('admission_date');
            $plaintiffs = $this->input->post('plaintiffs');
            $defendants = $this->input->post('defendants');
            $scheduled_action = $this->input->post('scheduled_action');

            $litigant = $this->litigant_model->get_litigant(array('litigants.dispute_no'=>$dispute_no,'litigants.id<>'=>$id));

            if(empty($dispute_no)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या कम्प्यूटरीकृत वाद संख्या लिखें !']); 	
                exit();
            }

            if($litigant){
                echo json_encode(['status'=>403, 'message'=>'यह कम्प्यूटरीकृत वाद संख्या पहले से उपलध है  !']); 	
                exit();
            }

            if(empty($section)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या सेक्शन चयन करें !']); 	
                exit();
            }
            if(empty($acts)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या एक्ट चयन करें !']); 	
                exit();
            }
       
            if(empty($block)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या ब्लॉक चयन करें !']); 	
                exit();
            }

            if(empty($village_name)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या ग्राम चयन करें !']); 	
                exit();
            }
            if(empty($police_station)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या थाना का नाम लिखें !']); 	
                exit();
            }
            if(empty($admission_date)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या दाखिला तिथि चुनें !']); 	
                exit();
            }
            if(empty($plaintiffs)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या वादी  लिखें !']); 	
                exit();
            }
            if(empty($defendants)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या प्रतिवादी  लिखें !']); 	
                exit();
            }
            if(empty($scheduled_action)){
                echo json_encode(['status'=>403, 'message'=>'कृप्या अपेछित कार्यवाही लिखें !']); 	
                exit();
            }

            $data = array(
                'userID' => $this->session->userdata('id'),
                'section' => $section,
                'acts' => $acts,
                'dispute_no' => $dispute_no,
                'block' => $block,
                'village_name' => $village_name,
                'police_station' => $police_station,
                'admission_date' => $admission_date,
                'plaintiffs' => $plaintiffs,
                'defendants' => $defendants,
                'scheduled_action' => $scheduled_action,
            );

            $update = $this->litigant_model->update($data, $id);
            if($update){
              echo json_encode(['status'=>200, 'message'=>'Litigant Update successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}
    }

    public function litigantReport(){
     $id     = $this->input->post('id');
     $remark = $this->input->post('remark');
     $get_remark = $this->litigant_model->get_litigant(array('id' => $id));
     if(empty($remark)){
        echo json_encode(['status'=>403, 'message'=>'कृप्या टिप्पणी दें!']);
        exit();
     }

    $this->load->library('upload');
    if($_FILES['image']['name']!= '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/remark',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp|pdf|txt',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/remark/'.$config['file_name'].'.'.$type;
      }
     }else{
        if(!empty($get_remark)){
            $image = $get_remark->lekhpal_upload;
        }else{
            echo json_encode(['status'=>403, 'message'=>'कृप्या आख्या अपलोड  करे!']);
            exit();
        }
        
	}

	$data = array(
		'lekhpal_remark' => $remark,
		'lekhpal_upload' => $image,
        'approve_status' => 2,
	);
    
    $update = $this->litigant_model->update($data, $id);

	if($update){
		echo json_encode(['status'=>200, 'message'=>'सफलतापूर्वक आख्या जमा !']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}
    }

    public function delete(){
		$id = $this->input->post('id');
		$delete = $this->litigant_model->delete($id);
		if($delete){
			echo json_encode(['status'=>200, 'message'=>'Litigant_model Delete successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
		}
	  }

      public function get_litigant(){
        $id = $this->input->post('id'); 
    $remarks = $this->litigant_model->get_litigant(array('id' => $id));
        //print_r($remarks);
     ?>
      <?php 
  if($remarks->approve_status==3){
 ?>
 <div class="mb-2">
  <div class="form-group mb-3 position-relative check-valid">
    <div class="input-group input-group-lg">
      <!-- <span class="input-group-text text-theme border-end-0"></span> -->
      <div class="form-floating">
        <textarea type="text" 
          class="form-control border-start-0" readonly><?=$remarks->disapprove_reason?></textarea>
        <label>निरस्त का कारन:</label>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<div class="mb-2">
  <div class="form-group mb-3 position-relative check-valid">
    <div class="input-group input-group-lg">
      <!-- <span class="input-group-text text-theme border-end-0">Remark:</span> -->
      <div class="form-floating">
        <p><?=$remarks->lekhpal_remark?></p>

      </div>
    </div>
  </div>
</div>

<div class="mb-2">
  <div class="input-group input-group-lg">
    <div class="form-floating">
      <?php 
                $file_extention = new SplFileInfo($remarks->lekhpal_upload);
                $extension = pathinfo($file_extention, PATHINFO_EXTENSION);
                if($extension == 'pdf'){ ?>
      <a class="test_link" href="<?=base_url($remarks->lekhpal_upload)?>" target="_blank">
        View PDF <i class="bi bi-file-earmark-pdf"></i>
      </a>
      <?php }else{ ?>
      <a href="<?=base_url($remarks->lekhpal_upload)?>" target="_blank"><img style="width: 17%;"
          src="<?=base_url($remarks->lekhpal_upload)?>"></img></a>
      <?php  }
    ?>
    </div>

  </div>
  <?php
           if(($this->session->userdata('user_type')=='उपयोगकर्ता' || $this->session->userdata('user_type')=='व्यवस्थापक') && $remarks->approve_status == 2){
          ?>
  <a href="javascript:void(0)" class="btn btn-success" id="approvelitigants" onclick="getStatusApproveModal(<?=$id?>)"
    data-toggle="modal" title="वाद पर टिप्पणी">आख्या को मंजूरी दें<i class="text-success bi bi-check-circle h5"></i></a>
  <a href="javascript:void(0)" class="btn btn-danger" onclick="getStatusDisapproveModal(<?=$id?>)" data-toggle="modal"
    title="वाद पर टिप्पणी">आख्या को अस्वीकार करें<i class="bi-file-plus"></i></a>

  <?php } ?>
</div>

<?php  
if($remarks->approve_status==1){
  ?>
  <h3>उपयोगकर्ता  द्वारा अपलोड  की गयी फाइल </h3>
  <div class="mb-2">
  <div class="form-group mb-3 position-relative check-valid">
    <div class="input-group input-group-lg">
      <!-- <span class="input-group-text text-theme border-end-0">Remark:</span> -->
      <div class="form-floating">
        <p><?=$remarks->user_remark?></p>

      </div>
    </div>
  </div>
</div>

<div class="mb-2">
  <div class="input-group input-group-lg">
    <div class="form-floating">
      <?php 
                $file_extention = new SplFileInfo($remarks->user_upload);
                $extension = pathinfo($file_extention, PATHINFO_EXTENSION);
                if($extension == 'pdf'){ ?>
      <a class="test_link" href="<?=base_url($remarks->user_upload)?>" target="_blank">
        View PDF <i class="bi bi-file-earmark-pdf"></i>
      </a>
      <?php }else{ ?>
      <a href="<?=base_url($remarks->user_upload)?>" target="_blank"><img style="width: 17%;"
          src="<?=base_url($remarks->user_upload)?>"></img></a>
      <?php  }
    ?>
    </div>

  </div>
  
</div>
  <?php
}
}

public function get_litigant_edit(){
    $id = $this->input->post('id'); 
    $remarks = $this->litigant_model->get_litigant(array('id' => $id));
    //print_r($remarks);
 ?>

<div class="mb-2">
  <div class="form-group mb-3 position-relative check-valid">
    <div class="input-group input-group-lg">
      <span class="input-group-text text-theme border-end-0">टिप्पणी:</span>
      <div class="form-floating">
        <textarea type="text" name="remark" id="remark"
          class="form-control border-start-0"><?=$remarks->lekhpal_remark?></textarea>
        <!-- <label>टिप्पणी</label> -->
      </div>
    </div>
  </div>
</div>
<div class="mb-2">
  <div class="form-group mb-3 position-relative check-valid">
    <div class="input-group input-group-lg">
      <span class="input-group-text text-theme border-end-0">File:</span>
      <div class="form-floating">
        <input type="file" placeholder="फ़ाइल अपलोड करें" name="image" id="image" accept="application/pdf"
          class="form-control border-start-0">
        <label>फ़ाइल अपलोड करें</label>
      </div>
    </div>
  </div>
</div>
<input type="hidden" name="id" id="id" value="<?=$id?>">
<?php  
}

public function download_sample(){
    $this->load->helper('download');
    $pth  = file_get_contents(base_url('uploads/sample_csv/litigants.csv'));
    $nme   =  "sample_litigants.csv";
    force_download($nme ,$pth);    
}

public function activeLitigants(){
    $id     = $this->input->post('id');
    $remark = $this->input->post('acremark');
    $get_remark = $this->litigant_model->get_litigant(array('id' => $id));
    if(empty($remark)){
       echo json_encode(['status'=>403, 'message'=>'कृप्या टिप्पणी दें!']);
       exit();
    }

   $this->load->library('upload');
   if($_FILES['image']['name']!= '')
       {
   $config = array(
     'upload_path' 	=> 'uploads/remark',
     'file_name' 	=> str_replace(' ','',$name).uniqid(),
     'allowed_types' => 'jpg|jpeg|png|gif|webp|pdf|txt',
     'max_size' 		=> '10000000',
   );
       $this->upload->initialize($config);
   if ( ! $this->upload->do_upload('image'))
     {
         $error = $this->upload->display_errors();
         echo json_encode(['status'=>403, 'message'=>$error]);
         exit();
     }else
     {
       $type = explode('.',$_FILES['image']['name']);
       $type = $type[count($type) - 1];
       $image = 'uploads/remark/'.$config['file_name'].'.'.$type;
     }
    }else{
       echo json_encode(['status'=>403, 'message'=>'कृप्या आख्या अपलोड  करे!']);
     exit();
  }

    $data = array(
        'user_remark' => $remark,
        'user_upload' => $image,
        'approve_status' => 1,
    );

    $update = $this->litigant_model->update($data, $id);

    if($update){
        echo json_encode(['status'=>200, 'message'=>'आख्या सक्रिय सफलतापूर्वक !']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
    }
}

public function deactiveLitigants(){
    $id     = $this->input->post('id');
    $remark = $this->input->post('deremark');
    $get_remark = $this->litigant_model->get_litigant(array('id' => $id));
    if(empty($remark)){
       echo json_encode(['status'=>403, 'message'=>'कृप्या टिप्पणी दें!']);
       exit();
    }

    $data = array(
        'disapprove_reason' => $remark,
        'approve_status' => 3,
    );

    $update = $this->litigant_model->update($data, $id);

    if($update){
        echo json_encode(['status'=>200, 'message'=>'आख्या निष्क्रिय सफलतापूर्वक !']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
    }
}

}